# config valid only for Capistrano 3.1
lock '3.2.1'

set :application, 'roadmojo-mvp'
set :repo_url, 'git@bitbucket.org:arijitraja/roadmojo.git'
set :stages, %w(production staging)
set :default_stage, "staging"
set :branch, 'trips'

set :deploy_to, '/home/ubuntu/roadmojo/roadmojo-mvp'
set :use_sudo, false

set :rvm_type, :user
set :rvm_ruby_version, '2.1.0'

set :keep_releases, 2

namespace :deploy do

  desc 'Restart application'
  task :restart do
    on roles(:app), in: :sequence, wait: 5 do
      execute("touch #{release_path.join('tmp/restart.txt')}")
    end
  end

  task :invoke do
    on roles(:app) do
      execute("ln -s #{shared_path}/database.yml #{release_path}/config/database.yml")
      execute("ln -s #{shared_path}/system #{release_path}/public/system")
      #execute("sudo /etc/init.d/elasticsearch start")
    end
  end

  before "deploy:compile_assets", :invoke
  after :publishing, :restart

end
