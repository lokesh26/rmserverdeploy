Rails.application.routes.draw do
  
  mount RailsAdmin::Engine => '/admin', as: 'rails_admin'
  devise_for :users, :controllers => { omniauth_callbacks: 'omniauth_callbacks' }, skip: [:sessions, :registrations]

  match '*all', to: 'application#allow_cors', via: [:options]
  # match "/auth/:provider/callback", to: "sessions#omniauth_session", via: [:get, :post]
  get "twitter_feeds" => "pages#twitter_feeds"

  devise_scope :user do
    post 'users' => 'users#create', :as => :user_signup
    resources :users do
      collection do
        get :my_trips
        get :my_drafts
        get :feed 
        get :followers
        get :following
        get :following_locations
        get :notifications
        get :all_notifications
        put :mark_as_read_notifications
        put :mark_as_unread_notifications
        get :suggest_users
      end
      member do
        post :upload_image
        get :liked_trips
        get :feed
        get :trips
        get :follow
        get :unfollow
        post :change_password
        post :update_username
        post :invite
        post :notification_setting
        get :following_people
        get :following_places
        get :followers_other
      end
    end
    get 'search' => 'pages#search'
    get 'users/:code/confirm_email' => 'users#confirm_email', as: :confirm_email
    post "users/forgot_password" => "users#forgot_password", as: :forgot_password
    post "users/toggle_profile_url_status/:provider" => "users#toggle_profile_url_status", as: :toggle_profile_url_status
    post "users/:code/reset_password" => "users#reset_password", as: :reset_password
    match 'users/:id', to: 'users#update', via: [:options, :put]
    resources :sessions, only: [:create]
    match 'sessions', to: "sessions#destroy", via: [:options, :delete], as: :logout

    resources :locations do
      member do
        get :follow
        get :unfollow
      end
    end

    resources :trips do
      resources :milestones
      resources :comments
      member do
        post :save_as_draft
        post :publish_trip
        get :like
        get :unlike
        post :upload_image
      end
      collection do
        get :random
        get :sort
        get :trending_trips
        get :nearby_trips
        post :explore
        get :explore_location
        get :searchtrips
      end
    end
    post "trips/:trip_id/moments" => "moments#create_via_trip", as: :create_via_trip
    post "comments/:comment_id/report_abuse" => "comments#report_abuse"

    resources :milestones do
      member do
        post :upload_image
        delete :delete_image
      end
      resources :moments
    end
    
    resources :moments do
      member do
        post :upload_image
        delete :delete_image
      end
    end
    
    resources :categories, only: [:index]
  end
  match '*all', to: 'application#bad_route', via: [:get, :put, :post, :delete, :options]

end