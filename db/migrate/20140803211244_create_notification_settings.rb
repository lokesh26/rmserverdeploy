class CreateNotificationSettings < ActiveRecord::Migration
  def change
    create_table :notification_settings do |t|
      t.integer :user_id
      t.boolean :default_as_miles, default: 1
      t.boolean :like, default: 1
      t.boolean :comment, default: 1
      t.boolean :follow, default: 1
      t.boolean :weekly_digest, default: 1
      t.timestamps
    end
  end
end
