class CreateTrips < ActiveRecord::Migration
  def change
    create_table :trips do |t|
      t.string :title
      t.text :description
      t.integer :viewcount
      t.integer :duration
      t.decimal :distance
      t.boolean :is_published
      t.string :transport_mode
      t.integer :user_id
      t.timestamps
    end
  end
end
