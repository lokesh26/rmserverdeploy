class ChangeIsPublishedAndDraftDefaultValues < ActiveRecord::Migration
  def up
    change_column :trips, :is_published, :boolean, default: false
    change_column :trips, :as_draft, :boolean, default: true
  end

  def down
    change_column :trips, :is_published, :boolean, default: nil
    change_column :trips, :as_draft, :boolean, default: false
  end
end
