class MakeMomentPolymorphic < ActiveRecord::Migration
  def change
    rename_column :moments, :milestone_id, :momentable_id
    add_column :moments, :momentable_type, :string
  end
end
