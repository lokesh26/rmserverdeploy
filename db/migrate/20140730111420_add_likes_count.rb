class AddLikesCount < ActiveRecord::Migration
  def self.up
    add_column :trips, :likes_count, :integer, :default => 0
    Trip.reset_column_information
    Trip.all.each do |t|
      Trip.reset_counters t.id, :likes
      t.update_attribute :likes_count, t.likes.length
    end
  end
  
  def self.down
    remove_column :trips, :likes_count
  end
end
