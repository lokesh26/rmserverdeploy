class AlterMoments < ActiveRecord::Migration
  def change
    add_column :moments, :location_id, :integer
    add_column :moments, :after_milestone, :integer
    add_column :moments, :category_id, :integer
    add_column :moments, :featured, :boolean
  end
end
