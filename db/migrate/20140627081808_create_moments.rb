class CreateMoments < ActiveRecord::Migration
  def change
    create_table :moments do |t|
      t.date :start_date
      t.date :end_date
      t.text :description
      t.string :title
      t.integer :milestone_id
      t.timestamps
    end
  end
end
