class CreateNotifications < ActiveRecord::Migration
  def change
    create_table :notifications do |t|
      t.integer :user_id
      t.text :description
      t.boolean :is_read
      t.integer :actor_id
      t.integer :action_id
      t.string :action_type
      t.timestamps
    end
  end
end
