class CreateImages < ActiveRecord::Migration
  def change
    create_table :images do |t|
      t.attachment :attachment
      t.integer :source_id
      t.string :source_type
      t.timestamps
    end
  end
end
