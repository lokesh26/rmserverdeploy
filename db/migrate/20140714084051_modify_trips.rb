class ModifyTrips < ActiveRecord::Migration
  def change
    change_table :trips do |t|
      t.datetime :start_date
      t.datetime :end_date
    end
    change_table :moments do |t|
      t.remove :start_date
      t.remove :end_date
    end
  end
end
