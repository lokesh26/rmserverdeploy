class AddSlugToUserAndTrip < ActiveRecord::Migration
  def change
    add_column :users, :slug, :string
    add_index :users, :slug
    add_column :trips, :slug, :string
    add_index :trips, :slug
  end
end
