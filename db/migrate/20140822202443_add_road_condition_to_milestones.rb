class AddRoadConditionToMilestones < ActiveRecord::Migration
  def change
    add_column :milestones, :road_condition, :integer, limit: 1
  end
end
