class AddAsDraftToTrips < ActiveRecord::Migration
  def change
    add_column :trips, :as_draft, :boolean, default: false
  end
end
