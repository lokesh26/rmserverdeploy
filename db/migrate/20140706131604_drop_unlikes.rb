class DropUnlikes < ActiveRecord::Migration
  def self.up
    drop_table :unlikes
  end

  def self.down
    create_table :unlikes do |t|
      t.integer :user_id
      t.integer :trip_id
      t.timestamps
    end
  end
end
