class CreateReportAbuses < ActiveRecord::Migration
  def change
    create_table :report_abuses do |t|
      t.integer :comment_id
      t.integer :user_id
      t.integer :reason, limit: 1
      t.string :remarks
      t.timestamps
    end
    
    add_column :comments, :is_abused, :boolean, default: false
    
  end
end
