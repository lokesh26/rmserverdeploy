class CreateMilestones < ActiveRecord::Migration
  def change
    create_table :milestones do |t|
      t.float :latitude
      t.float :longitude
      t.integer :location_id
      t.integer :trip_id
      t.date :reached_on
      t.integer :position
      t.boolean :featured, default: false
      t.text :description
      t.timestamps
    end
  end
end
