class AddPublishedAtToTrips < ActiveRecord::Migration
  def change
    add_column :trips, :published_at, :datetime
  end
end
