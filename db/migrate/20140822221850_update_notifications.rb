class UpdateNotifications < ActiveRecord::Migration
  def change
    rename_column :notifications, :user_id, :subject_id
    add_column :notifications, :subject_type, :string
    change_column :notifications, :is_read, :boolean, default: false
    remove_column :notifications, :description
  end
end
