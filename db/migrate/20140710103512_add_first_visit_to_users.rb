class AddFirstVisitToUsers < ActiveRecord::Migration
  def change
    add_column :users, :first_visit, :boolean, default: true
  end
end
