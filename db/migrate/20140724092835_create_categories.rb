class CreateCategories < ActiveRecord::Migration
  def up
    create_table :categories do |t|
      t.string :name
      t.integer :parent_id
      t.timestamps
    end
    
    get_categories.each do |category|
      parent = Category.create(name: category[0])
      category[1].each do |sub_cat|
        Category.create(name: sub_cat[1], parent_id: parent.id)
      end
    end
  end
  
  def get_categories
    return {
      "Stay" => {
        "1" => "Friends and Family",
        "2" => "BnBs & lnns",
        "3" => "Campsites",
        "4" => "Cottages & Cabins",
        "5" => "Hostels",
        "6" => "Hotels & Motels",
        "7" => "Spas & Resorts",
        "8" => "RV Parks",
        "9" => "Vacation Rentals",
        "10" => "Other",
        "11" => "Friends and Family",
        "12" => "BnBs & lnns",
      },
      "Eat/Drink" => {
        "1" => "Cafes & Bakeries",
        "2" => "Roadside/Highway Joints",
        "3" => "Restaurants & Diners",
        "4" => "Drive-ins",
        "5" => "Fast Food/ Take away",
        "6" => "Fine Dining",
        "7" => "Vegan & Vegetarian",
        "8" => "Speciality Foods",
        "9" => "Bars & Pubs",
        "10" => "Breweries, Vineyards & Distilleries",
        "11" => "Other"  
      },
      "See" =>{
        "0" => "Museums",
        "1" => "Galleries",
        "2" => "Monuments",
        "3" => "Historical Attractions",
        "4" => "Natural Features",
        "5" => "Other"
      },
      "Do" => {
        "1" => "Beaches",
        "2" => "Lakes & Forests",
        "3" => "Parks & Gardens",
        "4" => "Wild life and Zoos",
        "5" => "Shopping",
        "6" => "Volunteering",
        "7" => "Adventure, Water & Winter activities",
        "8" => "Other activities"
      },
      "Experience" => {
        "1" => "Amusement Parks",
        "2" => "Concerts and events",
        "3" => "Nightlife",
        "4" => "Theatres, Musicals & other live shows",
        "5" => "Festivals",
        "6" => "Other"
      }
    }
  end
  
  
  def down
    drop_table :categories
  end
end