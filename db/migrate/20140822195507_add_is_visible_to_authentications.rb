class AddIsVisibleToAuthentications < ActiveRecord::Migration
  def change
    add_column :authentications, :is_visible, :boolean, default: true
  end
end
