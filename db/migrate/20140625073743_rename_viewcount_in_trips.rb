class RenameViewcountInTrips < ActiveRecord::Migration
  def change
    rename_column :trips, :viewcount, :view_count
  end
end
