class AddDistanceCoveredToUsers < ActiveRecord::Migration
  def up
    add_column :users, :distance_covered, :decimal, default: 0.0
    change_column_default :trips, :distance, 0.0
  end
  
  def down
    remove_column :users, :distance_covered
    change_column_default :trips, :distance, nil
  end
end
