class RemoveAsDraftFromTrip < ActiveRecord::Migration
  def change
    remove_column :trips, :as_draft
  end
end
