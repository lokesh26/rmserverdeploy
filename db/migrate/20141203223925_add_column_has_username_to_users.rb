class AddColumnHasUsernameToUsers < ActiveRecord::Migration
  def change
  	add_column :users, :has_username, :boolean, default: true
  end
end
