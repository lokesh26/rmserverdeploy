class RemoveLatitudeFromMilestone < ActiveRecord::Migration
  def change
    change_table :milestones do |t|
      t.remove :latitude
      t.remove :longitude
    end
  end
end
