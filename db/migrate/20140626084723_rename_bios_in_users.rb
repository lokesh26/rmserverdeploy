class RenameBiosInUsers < ActiveRecord::Migration
  def change
    rename_column :users, :bios, :bio
  end
end
