# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20141203223925) do

  create_table "authentications", force: true do |t|
    t.integer  "user_id"
    t.string   "provider"
    t.string   "uid"
    t.string   "link"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.boolean  "is_visible", default: true
  end

  create_table "categories", force: true do |t|
    t.string   "name"
    t.integer  "parent_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "comments", force: true do |t|
    t.text     "body"
    t.integer  "user_id"
    t.integer  "trip_id"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.boolean  "is_abused",  default: false
  end

  create_table "delayed_jobs", force: true do |t|
    t.integer  "priority",   default: 0, null: false
    t.integer  "attempts",   default: 0, null: false
    t.text     "handler",                null: false
    t.text     "last_error"
    t.datetime "run_at"
    t.datetime "locked_at"
    t.datetime "failed_at"
    t.string   "locked_by"
    t.string   "queue"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "delayed_jobs", ["priority", "run_at"], name: "delayed_jobs_priority", using: :btree

  create_table "friendly_id_slugs", force: true do |t|
    t.string   "slug",                      null: false
    t.integer  "sluggable_id",              null: false
    t.string   "sluggable_type", limit: 50
    t.string   "scope"
    t.datetime "created_at"
  end

  add_index "friendly_id_slugs", ["slug", "sluggable_type", "scope"], name: "index_friendly_id_slugs_on_slug_and_sluggable_type_and_scope", unique: true, using: :btree
  add_index "friendly_id_slugs", ["slug", "sluggable_type"], name: "index_friendly_id_slugs_on_slug_and_sluggable_type", using: :btree
  add_index "friendly_id_slugs", ["sluggable_id"], name: "index_friendly_id_slugs_on_sluggable_id", using: :btree
  add_index "friendly_id_slugs", ["sluggable_type"], name: "index_friendly_id_slugs_on_sluggable_type", using: :btree

  create_table "identities", force: true do |t|
    t.integer  "user_id"
    t.string   "provider"
    t.string   "uid"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "identities", ["user_id"], name: "index_identities_on_user_id", using: :btree

  create_table "images", force: true do |t|
    t.string   "attachment_file_name"
    t.string   "attachment_content_type"
    t.integer  "attachment_file_size"
    t.datetime "attachment_updated_at"
    t.integer  "source_id"
    t.string   "source_type"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "likes", force: true do |t|
    t.integer  "user_id"
    t.integer  "trip_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "locations", force: true do |t|
    t.string   "name"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.float    "latitude"
    t.float    "longitude"
  end

  create_table "milestones", force: true do |t|
    t.integer  "location_id"
    t.integer  "trip_id"
    t.date     "reached_on"
    t.integer  "position"
    t.boolean  "featured",                 default: false
    t.text     "description"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "title"
    t.integer  "road_condition", limit: 1
  end

  create_table "moments", force: true do |t|
    t.text     "description"
    t.string   "title"
    t.integer  "momentable_id"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "momentable_type"
    t.integer  "location_id"
    t.integer  "after_milestone"
    t.integer  "category_id"
    t.boolean  "featured"
  end

  create_table "notification_settings", force: true do |t|
    t.integer  "user_id"
    t.boolean  "default_as_miles", default: true
    t.boolean  "like",             default: true
    t.boolean  "comment",          default: true
    t.boolean  "follow",           default: true
    t.boolean  "weekly_digest",    default: true
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "notifications", force: true do |t|
    t.integer  "subject_id"
    t.boolean  "is_read",      default: false
    t.integer  "actor_id"
    t.integer  "action_id"
    t.string   "action_type"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "subject_type"
  end

  create_table "relationships", force: true do |t|
    t.integer  "follower_id"
    t.integer  "followed_id"
    t.string   "followed_type"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "report_abuses", force: true do |t|
    t.integer  "comment_id"
    t.integer  "user_id"
    t.integer  "reason",     limit: 1
    t.string   "remarks"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "trips", force: true do |t|
    t.string   "title"
    t.text     "description"
    t.integer  "view_count"
    t.integer  "duration"
    t.decimal  "distance",       precision: 10, scale: 0, default: 0
    t.boolean  "is_published",                            default: false
    t.string   "transport_mode"
    t.integer  "user_id"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.datetime "start_date"
    t.datetime "end_date"
    t.integer  "likes_count",                             default: 0
    t.string   "slug"
    t.boolean  "is_private",                              default: false
    t.datetime "published_at"
  end

  add_index "trips", ["slug"], name: "index_trips_on_slug", using: :btree

  create_table "users", force: true do |t|
    t.string   "name"
    t.string   "email"
    t.text     "bio"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.boolean  "is_active",                                   default: false
    t.string   "auth_token"
    t.string   "confirmation_code"
    t.integer  "location_id"
    t.string   "weburl"
    t.boolean  "first_visit",                                 default: true
    t.string   "username"
    t.string   "reset_code"
    t.decimal  "distance_covered",   precision: 10, scale: 0, default: 0
    t.string   "slug"
    t.string   "encrypted_password",                          default: "",    null: false
    t.boolean  "has_username",                                default: true
  end

  add_index "users", ["slug"], name: "index_users_on_slug", using: :btree

end
