# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :trip do
    title "Test Trip"
    description "test"
    view_count "2"
    duration "20 days"
    distance "2"
    is_published true
    transport_mode "Car"
    user_id 1
  end
end
