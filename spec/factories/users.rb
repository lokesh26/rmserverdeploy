FactoryGirl.define do

	sequence :email do |n|
    "user#{n}@roadmojo.com"
  end 

  sequence :username do |n|
    "user#{n}-roadmojo.com"
  end 

  factory :user do
    username { generate(:username) }
    email { generate(:email) }
    password "qqqqqq"
    password_confirmation "qqqqqq"
  end
end
