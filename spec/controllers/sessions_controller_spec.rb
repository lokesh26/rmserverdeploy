require 'spec_helper'

describe SessionsController do

  describe "create" do
     it "shoud return 401 if user credentials dont match" do
      Notifier.stub(:confirmation_email).and_return(@object)
      @object.stub(:deliver).and_return(true)
      @user = FactoryGirl.create(:user, email: "test@test.com", password: "password", password_confirmation: "password")
      @request.env["devise.mapping"] = Devise.mappings[:user]
      post :create, {email: "test@example.com", password: 'password123', format: :json}
      response.status.should eq(401)

     end
     it "shoud return 200 if user credentials  match" do
      Notifier.stub(:confirmation_email).and_return(@object)
      @object.stub(:deliver).and_return(true)
      @user = FactoryGirl.create(:user, email: "test@test.com", password: "password", password_confirmation: "password")
      @request.env["devise.mapping"] = Devise.mappings[:user]
      post :create, {email: "test@example.com", password: 'password', format: :json}
      response.status.should eq(401)

     end      
  end
  
  describe "create" do
    
    before do
      @object = Object.new
      Notifier.stub(:confirmation_email).and_return(@object)
      @object.stub(:deliver).and_return(true)
      @user = FactoryGirl.create(:user)

      @suggested_location = FactoryGirl.create(:location)
      @suggested_users = FactoryGirl.create(:user)
      @suggested_trips = FactoryGirl.create(:trip)
      @suggested_locations = [@suggested_location]
      @suggested_users = [@suggested_user]
      @suggested_trips = [@suggested_trip]
      @params = {"email" => "test@example.com", "password" => "aaaaaa"}

      User.stub(:find_by_email).and_return(@user)
      Location.stub(:suggested).and_return(@suggested_locations)
      Trip.stub(:suggested).and_return(@suggested_trips)

      @user.stub(:suggested_users).and_return(@suggested_users)
      @user.stub(:authenticate).and_return(true)
      @user.stub(:generate_auth_token).and_return(true)
      @user.stub(:first_visit).and_return(true)
      @user.stub(:update_attribute).and_return(true)
    end
    
    describe "success" do
      
      it "should find the user" do
        User.should_receive(:find_by_email).with("test@example.com").and_return(@user)
      end
      
      it "should authenticate user with password" do
        @user.should_receive(:authenticate).with("aaaaaa").and_return(true)
      end
      
      it "should generate auth_token for user" do
        @user.should_receive(:generate_auth_token).and_return(true)
      end
      
      it "should collect suggested locations if user visits for the first time" do
        @user.should_receive(:first_visit).and_return(true)
        Location.should_receive(:suggested).and_return(@suggested_locations)
      end

      it "should not collect suggested locations if user visits after first time" do
        @user.should_receive(:first_visit).and_return(false)
        Location.should_not_receive(:suggested)
      end
      
      it "should collect suggested users if user visit for the first time" do
        @user.should_receive(:first_visit).and_return(true)
        @user.should_receive(:suggested_users).and_return(@suggested_users)
      end
      
      it "should not collect suggested users if user visits after first time" do
        @user.should_receive(:first_visit).and_return(false)
        User.should_not_receive(:suggested)
      end
      
      it "should collect suggested trips if user visits for the first time" do
        @user.should_receive(:first_visit).and_return(true)
        Trip.should_receive(:suggested).and_return(@suggested_trips)
      end

      it "should not collect suggested trips if user visits after first time" do
        @user.should_receive(:first_visit).and_return(false)
        Trip.should_not_receive(:suggested)
      end
       

      after do
        @user.should_receive(:update_attribute).with(:first_visit, false).and_return(true)
        post :create, {email: "test@example.com", password: 'aaaaaa', format: :json}
        response.should be_success
        response.status.should be(200)
      end
      
    end
    
    describe "bad request" do
      
      before do
        @errors = Object.new
        @user.stub(:errors).and_return(@errors)
        @errors.stub(:messsages).and_return(@errors)
      end 
      
      it "should return error if user object is not found" do
        User.should_receive(:find_by_email).with("test@example.com").and_return(nil)
      end
      
      after do
        post :create, {email: "test@example.com", password: 'aaaaaa', format: :json}
        response.status.should be(401)
        expect(response.body).to eq( {errors: "Invalid credentials"}.to_json )
      end
      
    end
    
  end

end
