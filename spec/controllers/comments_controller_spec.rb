require 'spec_helper'

describe CommentsController do
  
  describe "create" do
    
    before do
      @comment = FactoryGirl.build(:comment)
      @comments = [@comment]
      @trip = FactoryGirl.create(:trip)
      @params = {"body" => "test", "user_id" => "2", "trip_id" => "1"}
      Trip.stub(:find_by_id).with("1").and_return(@trip)
      @trip.stub(:comments).and_return(@comments)
      @comments.stub(:new).and_return(@comment)
      @comment.stub(:user=).and_return(@user)
      @comment.stub(:save).and_return(true)
    end
    
    describe "success" do
      
      it "should find the trip" do
        Trip.should_receive(:find_by_id).with("1").and_return(@trip)
      end
      
      it "should return comment parameters to the comment object" do
        @trip.should_receive(:comments).and_return(@comments)
        @comments.should_receive(:new).and_return(@comment)
      end
      
      it "should assign user to the comment" do
        @comment.should_receive(:user=).and_return(@user)
      end
      
      it "should save comment object" do
        @comment.should_receive(:save).and_return(true)
      end
      
      after do
        post :create, {comment: @params, format: :json, trip_id: 1}
        response.should be_success
        response.status.should be(200)
      end
      
    end
    
    describe "bad request" do
      
      before do
        @errors = Object.new
        @comment.stub(:errors).and_return(@errors)
        @errors.stub(:messages).and_return(@errors)
      end
      
      it "should return error if comment object is not saved" do
        @comment.should_receive(:save).and_return(false)
      end
      
      after do
       post :create, {comment: @params, format: :json, trip_id: 1}
       response.status.should be(422)
       expect(response.body).to eq( {errors: @errors}.to_json ) 
      end
         
    end
       
  end

end
