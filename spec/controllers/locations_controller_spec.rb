require 'spec_helper'

describe LocationsController do
  
  describe "follow" do
  
    before do
      @object = Object.new
      Notifier.stub(:confirmation_email).and_return(@object)
      @object.stub(:deliver).and_return(true)
      @location = FactoryGirl.create (:location)
      @user = FactoryGirl.create(:user)
      @follow = FactoryGirl.build(:relationship)
      @follows = [@follow]
      controller.stub(:current_user).and_return(@user)
      Location.stub(:find_by_id).and_return(@location)
      @location.stub(:relationships).and_return(@follows)
      @follows.stub(:find_or_create_by).and_return(@follow)
    end
  
    describe "success" do
    
      it "should find the location" do
        Location.should_receive(:find_by_id).with("1").and_return(@location)
      end
    
      it "should find or create relationship with current location for follows" do
        @location.should_receive(:relationships).and_return(@follows)
        @follows.should_receive(:find_or_create_by).with(follower: @user).and_return(@follow)
      end
    
      after do
        get :follow, {id: 1, format: :json}
        response.should be_success
        response.status.should be(200)
      end
    
    end
    
  end

  describe "unfollow" do
  
    before do
      @object = Object.new
      Notifier.stub(:confirmation_email).and_return(@object)
      @object.stub(:deliver).and_return(true)
      @location = FactoryGirl.create (:location)
      @user = FactoryGirl.create(:user)
      @unfollow = FactoryGirl.create(:relationship)
      @unfollows = [@unfollow]
      controller.stub(:current_user).and_return(@user)
      Location.stub(:find_by_id).and_return(@location)
      @location.stub(:relationships).and_return(@unfollows)
      @unfollows.stub(:where).and_return(@unfollows)
      @unfollow.stub(:destroy).and_return(true)
    end
  
    describe "success" do
    
      it "should find the location" do
        Location.should_receive(:find_by_id).with("1").and_return(@location)
      end
    
      it "should find or create relationship with current user for unfollows" do
        @location.should_receive(:relationships).and_return(@unfollows)
        @unfollows.should_receive(:where).with(follower: @user).and_return(@unfollows)
      end
    
      it "should destroy unfollow" do
        @unfollow.should_receive(:destroy).and_return(true)
      end
    
      after do
        get :unfollow, {id: 1, format: :json}
        response.should be_success
        response.status.should be(200)
      end
    
    end
  
  end
  
end
