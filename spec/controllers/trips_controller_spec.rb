require 'spec_helper'

describe TripsController do
  
  describe "create" do
    
    before do
      @trip = FactoryGirl.build(:trip)
      @params = {"title" => "test trip", "description" => "test", "view_count" => "2", "duration" => "20 days", "distance" => "2", "is_published" => "true", "transport_mode" => "car", "user_id" => "1"}
      Trip.stub(:new).and_return(@trip)
      @trip.stub(:save).and_return(true)
    end
    
    describe "success" do
      
      it "should return trip parameters to the trip object" do
        Trip.should_receive(:new).and_return(@trip)
      end
      
      it "should save trip object" do
        @trip.should_receive(:save).and_return(true)
      end
      
      after do
        post :create, {trip: @params, format: :json}
        response.should be_redirect
        response.status.should be(302)
        
      end
      
    end
    
    describe "bad request" do
      
      before do
        @errors = Object.new
        @trip.stub(:errors).and_return(@errors)
        @errors.stub(:messages).and_return(@errors)
      end
    
      it "should return error if trip object is not saved" do
        @trip.should_receive(:save).and_return(false)
      end
    
      after do
        post :create, {trip: @params, format: :json}
        response.status.should be(422)
        expect(response.body).to eq( {errors: @errors}.to_json )
      end
      
    end
    
  end
  
  describe "show" do
     
    before do
      @trip = FactoryGirl.build(:trip)
      @params = {"id" => "1", format: :json}
    end
    
    describe "success" do
      
      it "should find the trip" do
        Trip.should_receive(:find_by_id).with("1").and_return(@trip)
      end
      
      after do
        @trip.should_receive(:increment!).and_return(true)
        get :show, @params
        response.should be_success
        response.status.should be(200)
      end
      
    end
    
    describe "bad request" do
      
      it "should return error if trip is not found" do
        Trip.should_receive(:find_by_id).with("1").and_return(false)
      end
      
      after do
        get :show, @params
        response.status.should be(404)
        expect(response.body).to eq({errors: "Trip not found"}.to_json)
      end
      
    end
    
  end
  
  describe "save_as_draft" do

    before do
      @trip = FactoryGirl.create(:trip)
      @params = {"id" => "1",format: :json}
      Trip.stub(:find_by_id).with("1").and_return(@trip)
      @trip.stub(:update_attribute).and_return(true)
    end

    describe "success" do

      it "should find the trip" do
        Trip.should_receive(:find_by_id).with("1").and_return(@trip)
      end

      it "should update_attribute of trip" do
        @trip.should_receive(:update_attribute).and_return(true)
      end

      after do
        get :save_as_draft, @params
        response.should be_success
        response.status.should be(200)
      end

    end

    describe "bad request" do

      it "should return error if trip is not found" do
        Trip.should_receive(:find_by_id).with("1").and_return(false)
      end

      after do
        get :save_as_draft, @params
        response.status.should be(404)
        expect(response.body).to eq({errors: "Trip not found"}.to_json)
      end

    end

  end
  
  describe "like" do
    
    before do
      @object = Object.new
      Notifier.stub(:confirmation_email).and_return(@object)
      @object.stub(:deliver).and_return(true)
      @trip = FactoryGirl.create(:trip)
      @like = FactoryGirl.build(:like)
      @user = FactoryGirl.create(:user)
      @likes = [@like]
      controller.stub(:current_user).and_return(@user)
      Trip.stub(:find_by_id).with("2").and_return(@trip)
      @trip.stub(:likes).and_return(@likes)
      @likes.stub(:find_or_create_by).and_return(@like)
    end
    
    describe "success" do
      
      it "should find the trip" do
        Trip.should_receive(:find_by_id).with("2").and_return(@trip)
      end
      
      it "should find or create like for the trip with current user" do
        @trip.should_receive(:likes).and_return(@likes)
        @likes.should_receive(:find_or_create_by).with(user: @user).and_return(@like)
      end
      
      after do
        get :like, id: "2", format: :json
        response.should be_success
        response.status.should be(200)
      end
         
    end
    
    describe "bad request" do
      
      before do
        @errors = Object.new
        @like.stub(:errors).and_return(@errors)
        @errors.stub(:messages).and_return(@errors)
      end
      
      it "should return error if trip is not found" do
        Trip.should_receive(:find_by_id).with("2").and_return(nil)
      end
      
      after do
        get :like, id: "2", format: :json
        response.status.should be(404)
        expect(response.body).to eq( {errors: "Trip not found"}.to_json )
      end
      
    end
    
  end
  
  describe "unlike" do
    
    before do
      @object = Object.new
      Notifier.stub(:confirmation_email).and_return(@object)
      @object.stub(:deliver).and_return(true)
      @trip = FactoryGirl.create(:trip)
      @unlike = FactoryGirl.create(:like)
      @user = FactoryGirl.create(:user)
      @likes = [@like]
      controller.stub(:current_user).and_return(@user)
      Trip.stub(:find_by_id).with("2").and_return(@trip)
      @trip.stub(:likes).and_return(@likes)
      @likes.stub(:find_by).and_return(@like)
    end
    
    describe "success" do
      
      it "should find the trip" do
        Trip.should_receive(:find_by_id).with("2").and_return(@trip)
      end
      
      it "should find or create unlike for the trip with current user" do
        @trip.should_receive(:likes).and_return(@likes)
        @likes.should_receive(:find_by).with(user: @user).and_return(@like)
      end
      
      after do
        get :unlike, id: "2", format: :json
        response.should be_success
        response.status.should be(200)
      end
         
    end
    
    describe "bad request" do
      
      before do
        @errors = Object.new
        @unlike.stub(:errors).and_return(@errors)
        @errors.stub(:messages).and_return(@errors)
      end
      
      it "should return error if trip is not found" do
        Trip.should_receive(:find_by_id).with("2").and_return(nil)
      end
      
      after do
        get :unlike, id: "2", format: :json
        response.status.should be(404)
        expect(response.body).to eq( {errors: "Trip not found"}.to_json )
      end
      
    end 
    
  end
  
  describe "random" do
    
    before do
      @trip = FactoryGirl.create(:trip)
      Trip.stub(:random).and_return(@trip)
    end
    
    describe "success" do
      
      it "should return a trip randomly" do
        Trip.should_receive(:random).and_return(@trip)
      end
      
      after do
        get :random, format: :json
        response.should be_success
        response.status.should be(200)
      end
      
    end
    
    describe "bad request" do
      
      before do
        @errors = Object.new
        @trip.stub(:errors).and_return(@errors)
        @errors.stub(:messages).and_return(@errors)
      end
      
      it "should return error if trip not found" do
        Trip.should_receive(:random).and_return(nil)        
      end
      
      after do
        get :random, format: :json
        response.status.should be(404)
        expect(response.body).to eq( {errors: "Trip not found"}.to_json )
      end
      
    end
    
  end
  
  describe "upload_image" do
    
    before do
      @trip = FactoryGirl.create(:trip)
      @image = FactoryGirl.build(:image)
      @images = [@image]
      @params = {"attachment" => "attachment"}
      Trip.stub(:find_by_id).and_return(@trip)
      @trip.stub(:images).and_return(@images)
      @images.stub(:build).and_return(@image)
      @image.stub(:save).and_return(true)
    end
    
    describe "success" do
      
      it "should find the trip" do
        Trip.should_receive(:find_by_id).with("2").and_return(@trip)
      end
      
      it "should initialize an image for the trip object" do
        @trip.should_receive(:images).and_return(@images)
        @images.should_receive(:build).and_return(@image)
      end
      
      it "should save image object" do
        @image.should_receive(:save).and_return(true)
      end
      
      after do
        get :upload_image, {id: "2", format: :json}
        response.should be_success
        response.status.should be(200)
      end
      
    end
    
    describe "bad request" do
      
      before do
        @errors = Object.new
        @images.stub(:errors).and_return(@images)
        @images.stub(:messages).and_return(@images)
      end
      
      it "should return error if trip is not found" do
        Trip.should_receive(:find_by_id).with("2").and_return(nil)
      end
      
      after do
        get :upload_image, {id: "2", format: :json}
        response.status.should be(404)
        expect(response.body).to eq( {errors: "Trip not found"}.to_json )
      end
      
    end
    
  end
  
  describe "update" do
    
    before do
      @object = Object.new
      Notifier.stub(:confirmation_email).and_return(@object)
      @object.stub(:deliver).and_return(true)
      @trip = FactoryGirl.create(:trip)
      @user = FactoryGirl.create(:user)
      @params = { "title" => "test trip", "description" => "test", "view_count" => "2", "duration" => "20 days", "distance" => "2", "is_published" => "true", "transport_mode" => "car"}
      Trip.stub(:find_by_id).and_return(@trip)
      controller.stub(:current_user).and_return(@user)
      @trip.stub(:update_attributes).and_return(@trip)
    end
    
    describe "success" do
      it "should find the trip" do
        Trip.should_receive(:find_by_id).with("1").and_return(@trip)
      end
      
      it "should update attributes of the trip" do
        @trip.should_receive(:update_attributes).with(@params.merge("user_id" => @user.id)).and_return(true)
      end
      
      after do
        put :update, { trip: @params, id: "1", format: :json }
        response.should be_success
        response.status.should be(200)
      end
       
    end
    
    describe "bad request" do
      
      before do
        @errors = Object.new
        @trip.stub(:errors).and_return(@errors)
        @errors.stub(:messages).and_return(@errors)
      end

      it "should return error if trip is not found" do
        Trip.should_receive(:find_by_id).with("1").and_return(nil)
        put :update, { trip: @params, id: "1", format: :json }
        response.status.should be(404)
        expect(response.body).to eq({errors: "Trip not found"}.to_json)
      end

      it "should return error if the trip object is not updated" do
        Trip.should_receive(:find_by_id).with("1").and_return(@trip)
        @trip.should_receive(:update_attributes).and_return(false)
        put :update, { trip: @params, id: "1", format: :json }
        response.status.should be(422)
        expect(response.body).to eq({errors: @errors}.to_json)
      end
          
    end
    
  end 
  
  describe "destroy" do
    
    before do
      @trip = FactoryGirl.create(:trip)
      @trips = [@trip]
      @params = {"id" => "1",format: :json}
      Trip.stub(:where).and_return(@trips)
      @trips.stub(:find_by_id).and_return(@trip)
      @trip.stub(:destroy).and_return(true)
    end
    
    describe "success" do
       
      it "should find draft trips" do
        Trip.should_receive(:where).with(as_draft: true).and_return(@trips)
      end
  
      it "should find the trip" do
        @trips.should_receive(:find_by_id).with("1").and_return(@trip)
      end
  
      it "should destroy the trip" do
        @trip.should_receive(:destroy).and_return(true)
      end
    
      after do
        delete :destroy, { trip: @params, id: "1", format: :json }
        response.should be_success
        response.status.should be(200)
      end
    
    end

    describe "bad request" do
    
      it "should return error if trip is not found" do
        @trips.should_receive(:find_by_id).with("1").and_return(false)
      end
    
      after do
        delete :destroy, { trip: @params, id: "1", format: :json }
        response.status.should be(404)
        expect(response.body).to eq({errors: "Trip not found"}.to_json)
      end
      
    end
    
  end
  
  describe "sort" do
    
    before do
      @object = Object.new
      Notifier.stub(:confirmation_email).and_return(@object)
      @object.stub(:deliver).and_return(true)
      @trip = FactoryGirl.create(:trip)
      @user = FactoryGirl.create(:user)
      @params = { "sort_by" => "created_at", "sort_type" => "desc"}
      @trips = [@trip]
      controller.stub(:current_user).and_return(@user)
      @user.stub(:feeds).and_return(@trips)
      @trips.stub(:order).and_return(@trips)
    end
    
    describe "success" do
      
      it "should get feeds from user" do
        @user.should_receive(:feeds).and_return(@trips)
      end
      
      it "should sort feed" do
        @trips.should_receive(:order).and_return(@trips)
      end
      
      after do
        get :sort, {sort: @params, format: :json}
        response.should be_success
        response.status.should be(200)
      end
      
    end
    
  end

end
