require 'spec_helper'

describe MilestonesController do

  describe "create" do
  
    before do
      @milestone = FactoryGirl.build(:milestone)
      @milestones = [@milestone]
      @trip = FactoryGirl.create(:trip)
      @params = {"latitude" => "66.20", "longitude" => "70.30", "location_id" => "1", "trip_id" => "2",
                 "reached_on" => Date.today, "position" => "1", "featured" => "true", "description" => "test"}
      Trip.stub(:find_by_id).with("2").and_return(@trip)
      @trip.stub(:milestones).and_return(@milestones)
      @milestones.stub(:new).and_return(@milestone)
      @milestone.stub(:save).and_return(true)
    end
  
    describe "success" do
    
      it "should find the trip" do
        Trip.should_receive(:find_by_id).with("2").and_return(@trip)
      end
     
      it "should return milestone parameters to the milestone object" do    
        @trip.should_receive(:milestones).and_return(@milestones)
        @milestones.should_receive(:new).and_return(@milestone)
      end
    
      it "should save milestone object" do
        @milestone.should_receive(:save).and_return(true)
      end
    
      after do
        post :create, {milestone: @params, format: :json, trip_id: 2}
        response.should be_success
        response.status.should be(200)
      end
     
    end
  
    describe "bad request" do
    
      before do
        @errors = Object.new
        @milestone.stub(:errors).and_return(@errors)
        @errors.stub(:messages).and_return(@errors)
      end
    
      it "should return error if milestone object is not saved" do
        @milestone.should_receive(:save).and_return(false)
      end
    
      after do
        post :create, {milestone: @params, format: :json, trip_id: 2}
        response.status.should be(422)
        expect(response.body).to eq( {errors: @errors}.to_json )
      end
    
    end
  
  end

end