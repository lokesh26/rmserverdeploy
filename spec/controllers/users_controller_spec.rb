require 'spec_helper'

describe UsersController do
  
  describe "create" do
    it "should create a new user" do
      mailer = double
      Notifier.should_receive(:confirmation_email).and_return(mailer)
      mailer.should_receive(:deliver)     
      post :create, user: {"username" => "test name", "email" => "test@example.com", "password" => "aaaaaa", "bio" => "some test description"}, format: :json
      response.should be_success
      response.status.should be(200)
      User.count.should eq(1)
    end

    it "should return 422 is validation error" do
      post :create, user: {"email" => "test@example.com", "password" => "aaaaaa", "bio" => "some test description"}, format: :json      
      response.status.should be(422)
      User.count.should eq(0)
    end
  end

  describe "show" do
    it "should return user information" do
      mailer = double
      Notifier.should_receive(:confirmation_email).and_return(mailer)
      mailer.should_receive(:deliver)     
      user = FactoryGirl.create(:user)
      get :show, {"id" => user.id, format: :json}
      response.should be_success
      response.status.should be(200)
    end

    it "should return 404 if user not found" do
      mailer = double
      Notifier.should_receive(:confirmation_email).and_return(mailer)
      mailer.should_receive(:deliver)     
      user = FactoryGirl.create(:user)
      get :show, {"id" => 100, format: :json}      
      response.status.should be(404)
      expect(response.body).to eq({errors: "User not found"}.to_json)
    end
  end

  describe "update" do

    it "should update user information" do
      mailer = double
      Notifier.should_receive(:confirmation_email).and_return(mailer)
      mailer.should_receive(:deliver)     
      user = FactoryGirl.create(:user)
      user.generate_auth_token
      params = { "name" => "updated test name", "email" => "test@example.com", "password" => "aman1710", "bio" => "test description" }
      request.headers["X-Auth-Token"] = user.auth_token
      put :update, { user: params, id: user.id, format: :json }   
      response.status.should be(200)     
      user.reload.email.should eq("test@example.com")
    end

    it "should return 404 if user not found" do
      mailer = double
      Notifier.should_receive(:confirmation_email).and_return(mailer)
      mailer.should_receive(:deliver)     
      user = FactoryGirl.create(:user)
      user.generate_auth_token
      params = { "name" => "updated test name", "email" => "test@example.com", "password" => "aman1710", "bio" => "test description" }
      request.headers["X-Auth-Token"] = user.auth_token
      put :update, { user: params, id: 100, format: :json }   
      response.status.should be(404)
      expect(response.body).to eq({errors: "User not found"}.to_json)
    end    
  end

  describe "forgot password" do
    it "should send email if user found" do
      mailer = double
      Notifier.should_receive(:confirmation_email).and_return(mailer)
      mailer.should_receive(:deliver)     
      user = FactoryGirl.create(:user)
      User.should_receive(:find_by_email).and_return(user)
      user.should_receive(:send_password_reset_instructions)
      post :forgot_password, email: user.email, format: :json
      response.status.should eq(200)
    end

    it "should send throw error response if user not found" do      
      post :forgot_password, email: "email", format: :json
      response.status.should eq(422)
    end
  end

  describe "mark_as_read_notifications" do

    it "should mark notifications as read" do
      mailer = double
      Notifier.should_receive(:confirmation_email).and_return(mailer)
      mailer.should_receive(:deliver)     
      user = FactoryGirl.create(:user)
      user.generate_auth_token      
      n1 = FactoryGirl.create(:notification, is_read: false, subject: user)
      n2 = FactoryGirl.create(:notification, is_read: false, subject: user)      
      request.headers["X-Auth-Token"] = user.auth_token
      put :mark_as_read_notifications, notification_ids: [n1.id, n2.id], format: :json       
      n1.reload.is_read.should be_true
      n2.reload.is_read.should be_true
    end
    
  end

  describe "my trips" do

    it "should return user trips" do
      mailer = double
      Notifier.should_receive(:confirmation_email).and_return(mailer)
      mailer.should_receive(:deliver)     
      user = FactoryGirl.create(:user)
      user.generate_auth_token
      trip = FactoryGirl.create(:trip, user: user)
      trip.update_attributes(is_published: true, published_at: Time.now)
      request.headers["X-Auth-Token"] = user.auth_token
      get :my_trips             
      response.status.should be(200)
      
      
    end
  end

  describe "follow" do

    it "should follow a different user" do
      mailer = double
      Notifier.should_receive(:confirmation_email).twice.and_return(mailer)
      mailer.should_receive(:deliver).twice
      user = FactoryGirl.create(:user)
      follow_user = FactoryGirl.create(:user)
      user.generate_auth_token
      request.headers["X-Auth-Token"] = user.auth_token
      get :follow, {id: follow_user.id, format: :json}
      response.status.should be(200)
      Relationship.count.should eq(1)
    end

    it "should not follow itself" do
      mailer = double
      Notifier.should_receive(:confirmation_email).and_return(mailer)
      mailer.should_receive(:deliver)
      user = FactoryGirl.create(:user)      
      user.generate_auth_token
      request.headers["X-Auth-Token"] = user.auth_token
      get :follow, {id: user.id, format: :json}
      response.status.should be(200)
      Relationship.count.should eq(0)
    end    
  end

  describe "unfollow" do

    it "should unfollow  user" do
      mailer = double
      Notifier.should_receive(:confirmation_email).twice.and_return(mailer)
      mailer.should_receive(:deliver).twice
      follow_user = FactoryGirl.create(:user)
      user = FactoryGirl.create(:user)      
      user.relationships.create(followed: follow_user)
      user.generate_auth_token
      request.headers["X-Auth-Token"] = user.auth_token
      get :unfollow, {id: follow_user.id, format: :json}
      response.status.should be(200)
      Relationship.count.should eq(0)
    end

    
  end
  
  
 
  
  
  describe "feed" do
    
    before do
      @trip = FactoryGirl.create(:trip)
      @trips = [@trip]
      @user = FactoryGirl.build(:user)
      controller.stub(:current_user).and_return(@user)
      @user.stub(:feeds).and_return(@trips)
    end
    
    describe "success" do
      
      it "should get current user" do
        controller.should_receive(:current_user).and_return(@user)
      end
      
      it "should return all feeds of the current user" do
        @user.should_receive(:feeds).and_return(@trips)
      end

      it "should return suggested locations and people if feed is blank" do
        @user.should_receive(:feeds).and_return([])
        Location.should_receive(:suggested).and_return([])
        @user.should_receive(:suggested_users).and_return([])
      end

      after do
        get :feed
        response.should be_success
        response.status.should be(200)
      end
      
    end
    
  end

end
