require 'spec_helper'

describe User do
  context "validations" do
    it { should validate_presence_of(:username) }
    it { should validate_presence_of(:email) }    

    it "should validate uniqueness of username"  do
    	mailer = double
    	Notifier.should_receive(:confirmation_email).and_return(mailer)
    	mailer.should_receive(:deliver)    	
    	FactoryGirl.create(:user, username: "test")
    	user = FactoryGirl.build(:user, username: "test")
    	user.save
    	user.errors.messages[:username].should eq(["has already been taken"])
    end

    it "should validate uniqueness of email"  do
    	mailer = double
    	Notifier.should_receive(:confirmation_email).and_return(mailer)
    	mailer.should_receive(:deliver)    	
    	FactoryGirl.create(:user, email: "test@test.com")
    	user = FactoryGirl.build(:user, email: "test@test.com")
    	user.save
    	user.errors.messages[:email].should eq(["has already been taken"])
    end
  end
  
  context "assosiation" do
    it { should have_many (:trips) }
  end

  describe "confirmation code" do
  	it "should generate conformation code when user is created" do
  		mailer = double
    	Notifier.should_receive(:confirmation_email).and_return(mailer)
    	mailer.should_receive(:deliver)    	
  		user = FactoryGirl.create(:user)
  		user.confirmation_code.should_not be_nil
  	end
  end

  describe "notification settings" do
  	it "should create notification_setting" do
  		mailer = double
    	Notifier.should_receive(:confirmation_email).and_return(mailer)
    	mailer.should_receive(:deliver)    	
  		user = FactoryGirl.create(:user)
  		NotificationSetting.where("user_id = ?", user.id).should_not be_nil
  	end
  end

  describe "send_password_reset_instructions" do

  	it "should send email to user with reset code" do
  		mailer = double
    	Notifier.should_receive(:confirmation_email).and_return(mailer)
    	mailer.should_receive(:deliver)    	
  		user = FactoryGirl.create(:user)
  		mailer2 = double
  		Notifier.should_receive(:send_password_reset_instructions).and_return(mailer2)
			mailer2.should_receive(:deliver)  		
  		user.send_password_reset_instructions
  		user.reload.reset_code.should_not be_nil  		
  	end




  end
  
end
