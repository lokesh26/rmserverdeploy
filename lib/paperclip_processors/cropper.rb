module Paperclip
  class Cropper < Thumbnail
    def transformation_command
      if crop_command
        super[3] = crop_command
      else
        super
      end
    end

    def crop_command
      target = @attachment.instance
      if target.cropping?
        #" -crop #{Paperclip::Geometry.from_file(target.photo.queued_for_write[:original])}+#{target.crop_x}+#{target.crop_y}"
        " -crop #{target.crop_w}x#{target.crop_h}+#{target.crop_x}+#{target.crop_y}"
      end
    end
  end
end
