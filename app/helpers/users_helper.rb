module UsersHelper

  def generate_notification_content(noti)
    hash = case noti.action_type
    when "Relationship"
      { actor: noti.actor.try(:username), action: "followed you on Roadmojo.", subject: "", subject_id: noti.try(:subject_id), actor_id: noti.try(:actor_id), created_at: time_ago_in_words(noti.try(:created_at)) }
    when "Comment"
      { actor: noti.actor.try(:username), action: "commented on your trip", subject: noti.action.try(:trip).try(:title), actor_id: noti.try(:actor_id), subject_id: noti.action.try(:trip_id), created_at: time_ago_in_words(noti.try(:created_at)) }
    when "Like"
      { actor: noti.actor.try(:username), action: "liked your trip", subject: noti.action.try(:trip).try(:title), actor_id: noti.try(:actor_id), subject_id: noti.action.try(:trip_id), created_at: time_ago_in_words(noti.try(:created_at)) }
    else
      {}
    end
      hash.merge!(is_read: noti.is_read, id: noti.id, actor_img_url: noti.actor.try(:image_url))
  end

end
