class TripsController < ApplicationController
  
  before_filter :load_trip, except:[:create, :random, :trending_trips, :sort, :nearby_trips, :explore, :index, :featured, :explore_location, :searchtrips]
  after_filter :update_view_count, only: [:show]
  skip_before_filter :authenticate_user, only: [:explore, :featured, :explore_location, :show, :trending_trips]

  def index
    @trips = Trip.published.limit(30)
    render 'users/trips'
  end

  def create
    @trip = Trip.new(trip_params)
    if @trip.save
      render :show
    else
      render_error_message(@trip)
    end
  end

  def show
    if @trip.is_published == false && @trip.user_id != current_user.try(:id)
      render json: {errors: "You don't have access"}, status: 400, head: "Bad request"
    end
  end
  
  def update
    if current_user && @trip.user_id == current_user.id
      if @trip.update_attributes(trip_params)
        render :show
      else
        render_error_message(@trip)
      end
    else
      render json: {errors: "Unauthorized"}, status: 400, head: "Bad request"
    end
  end
  
  def upload_image
    @image = @trip.images.build
    @image.attributes = {attachment: params[:attachment]}
    if @image.save
      render :show
    else
      render_error_message(@image)
    end
  end
  
  def like
    @like = @trip.likes.find_or_create_by(user: current_user)
  end
  
  def unlike
    @like = @trip.likes.find_by(user: current_user)
    @like.destroy if @like
    render 'like'
  end

  def save_as_draft
    if current_user && @trip.user_id == current_user.id
      @trip.is_published = false
      @trip.attributes = trip_params
      if @trip.save
        render :show
      else
        render_error_message(@trip)
      end
    else
      render json: {errors: "Unauthorized"}, status: 400, head: "Bad request"
    end
  end

  def featured
    @trips = Trip.featured
    render :index
  end
  
  def publish_trip
    if @trip && @trip.user_id == current_user.try(:id)
      @trip.published_at = Time.now unless @trip.published_at
      if @trip.update_attributes(trip_params)
        render json: {published: @trip.is_published, published_at: @trip.published_at, is_private: @trip.is_private}
      else
        render json: {errors: "There was some error. Trip can not be published.", status: 422, head: "Unprocessable Entity"}
      end
    else
      render json: {errors: "You dont have access to perform this action", status: 422, head: "Unprocessable Entity"}
    end
  end
  
  def destroy
    if @trip.user_id == current_user.try(:id)
      @trip.destroy
      render_nothing
    else
      render json: {errors: "You don't have access to delete this trip", status: 422, head: "Unprocessable Entity"}
    end
  end
  
  def sort
    @trips = current_user.feeds.order("#{params[:sort_by] || 'created_at'} #{params[:sort_type] || 'desc'}").limit(30)
    render 'users/trips'
  end
  
  def trending_trips
    @trips = Trip.cached_trending_trips(current_user).limit(params[:count] || 30)
    render 'users/trips'
  end
  
  def nearby_trips
    if params[:lat].present? && params[:lng].present?
      location_ids = Location.near([params[:lat], params[:lng]], 200).collect(&:id)
      trip_ids = Milestone.where(location_id: location_ids).collect(&:trip_id)
      @trips = Trip.where(id: trip_ids).where("user_id != ?", current_user.id).limit(30)
      render 'users/trips'
    else
      render json: {errors: "User's location not found", status: 422, head: "Unprocessable Entity"}
    end
  end

  def random
    @trip = Trip.random
    if @trip
      render :show
    else
      render_not_found('Trip')
    end
  end
  
  def explore
    begin
      if (ne = params[:ne]) && (sw = params[:sw]) && (center = params[:center])
        distance = Geocoder::Calculations.distance_between([ne[:latitude], ne[:longitude]], [sw[:latitude], sw[:longitude]])
        box = Geocoder::Calculations.bounding_box([center[:latitude] , center[:longitude]], distance)
        @locations = Location.within_bounding_box(box).select("distinct locations.*, COUNT(milestones.id) as milestone_count").joins(:milestones).group("locations.id").order("milestone_count DESC").limit(10)
        result = Location.get_trip_details
        @location, @recent_trip, @trip_ids, @trending_trips = result if result.present?
        render 'locations/index'
      else
        render json: {errors: "Wrong input parameters"}, status: 422, head: "Unprocessable Entity"
      end
    rescue
      render json: {errors: "There was some error"}, status: 422, head: "Unprocessable Entity"
    end
  end
  
  def explore_location
    result = Location.get_trip_details(params[:location_id])
    if result.present?
      @location, @recent_trip, @trip_ids, @trending_trips = result
    else
      render json: {errors: "Location not found", status: 422, head: "Unprocessable Entity"}
    end
  end

  def searchtrips
    query = params[:searchq]
    @trips = Trip.search "2% #{query}", fields: [:title, :description, :slug], boost_by: {view_count: {factor: 2}, likes_count: {factor: 10}}, include: [:user, :milestones => :location], where: {is_published: true, is_private: false}, limit: 30
    render "users/trips"
  end
  
  private
  def trip_params
    if params[:trip]
      params[:trip].merge!({user_id: current_user.try(:id)})
      params[:trip].permit(:title, :description, :start_point, :end_point, :start_date, :end_date, :transport_mode, :user_id,
                           :is_published, :is_private, :published_at, :duration)
    else
      {}
    end
  end

  def update_view_count
    @trip.increment!(:view_count) if @trip.is_published
  end
  
  def load_trip
    begin
      @trip = Trip.friendly.find(params[:id])
    rescue
      render_not_found('Trip')
    end
  end

end
