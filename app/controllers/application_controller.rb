class ApplicationController < ActionController::Base

  before_filter :allow_cors
  before_filter :authenticate_user, except: [:bad_route]

  def allow_cors    
    headers["Access-Control-Allow-Origin"] = "*"    
    headers["Access-Control-Allow-Methods"] = "GET, POST, PUT, DELETE, PATCH, OPTIONS, HEAD"
    headers["Access-Control-Allow-Headers"] = %w{Origin Accept Content-Type X-Requested-With X-CSRF-Token X-Auth-Token}.join(",")
    head(:ok) if request.request_method == "OPTIONS"
  end

  def bad_route
    render json: {errors: "Bad Route"}, status: 400, head: "Bad request"
  end

  def return_response(obj)
    render json: obj, status: 200, head: "OK"
  end
  
  def render_error_message(obj)
    render json: {errors: obj.errors.messages}, status: 422, head: "Unprocessable Entity"
  end
  
  def render_not_found(obj)
    render json: { errors: "#{obj} not found"}, status: 404, head: "Not Found"
  end

  def render_nothing
    render json: {}, status: 200, head: "OK"
  end
  
  def authenticate_user    
    unless current_user
      render json: {errors: "Unauthorized"}, status: 400, head: "Bad request"
    end
  end
  
  def current_user    
    @current_user ||= User.find_by_auth_token(request.headers["X-Auth-Token"]) if request.headers["X-Auth-Token"]
    @current_user ||= User.find_by_auth_token(params["X-Auth-Token"]) if @current_user.blank? && params["X-Auth-Token"].present?
    return @current_user
  end
  helper_method :current_user

end
