class UsersController < ApplicationController
  

  before_filter :load_user, except: [:create, :confirm_email, :my_trips, :feed, :followers, :following, :following_locations,
                                     :forgot_password, :reset_password, :my_drafts, :toggle_profile_url_status,
                                     :notifications,:all_notifications,
                                      :suggest_users, :mark_as_read_notifications,
                                      :mark_as_unread_notifications, :invite, :index]
                                     
  skip_before_filter :authenticate_user, only: [:create, :show, :forgot_password, :reset_password,
                       :confirm_email, :liked_trips, :followers_other, :following_places, :following_people, 
                       :update_username, :index]

  http_basic_authenticate_with name: "roadmojo", password: "R0@dm0j0", only: :index
  
  def index
    @users = User.all
    @trips = Trip.all
  end

  def create
    @user = User.new(user_params)
    if @user.save
      render :show
    else
      render_error_message(@user)
    end
  end
  
  def update
    if @user.update_attributes(user_params)
      render :show
    else
      render_error_message(@user)
    end
  end

  def update_username
    if @user.update_attributes(username: params[:username],has_username: true)
      render :show
    else
      render_error_message(@user)
    end
    
  end
  
  def forgot_password
    user = User.find_by_email(params[:email])
    if user
      user.send_password_reset_instructions
      render_nothing
    else
      render json: {errors: "Please enter a valid registered email"}, status: 422, head: "Unprocessable Entity"
    end
  end

  def reset_password
    user = User.find_by_reset_code(params[:code])
    if user
      user.attributes = reset_password_params
      if user.save
        user.update_attribute(:reset_code, nil)
        render json: { email: user.email }, status: 200, head: :ok
      else
        render_error_message(user)
      end
    else
      render json: {errors: "Invalid URL"}, status: 422, head: "Unprocessable Entity"
    end
  end

  def change_password
    if current_user == @user && current_user.valid_password?(params[:current_password]) && current_user.update_attributes(password: params[:new_password], password_confirmation: params[:new_password_confirmation])
      render :show
    else
      render json: {errors: "Password not updated"}, status: 422, head: "Unprocessable Entity"
    end
  end

  def invite
    if Notifier.delay.send_invite(current_user, params[:emails], params[:personalMessage])
      render json: {}, status: 200, head: :ok
    else
      render json: {errors: "Nope"}, status: 422, head: "Unprocessable Entity"
    end
  end
  
  def notification_setting
    if @user == current_user
      notification_setting = NotificationSetting.find_or_initialize_by(user_id: current_user.id)
      notification_setting.attributes = notification_setting_params
      if notification_setting.save
        render 'users/show'
      else
        render json: {errors: "Can't save notification stetting"}, status: 422, head: "Unprocessable Entity"
      end
    end
  end

  def all_notifications
    @notifications = current_user.notifications.where.not(actor_id: current_user.id)
    render :notifications
  end

  def notifications
    @notifications = current_user.notifications.where.not(actor_id: current_user.id).where(is_read: false)
  end


  def mark_as_read_notifications        
    current_user.notifications.where("id in (?)", params[:notification_ids]).where(is_read: false).update_all(is_read: true)  
    render_nothing
  end

  def mark_as_unread_notifications        
    current_user.notifications.update_all(is_read: false) 
    render_nothing
  end


  def toggle_profile_url_status
    @user = current_user
    provider = current_user.authentications.where("lower(provider) = ?", params[:provider].downcase).first
    if provider
      provider.update_attribute(:is_visible, params[:is_visible])
      render 'users/show'
    else
      render json: {errors: "Provider not found"}, status: 404, head: 'Not Found'
    end
  end

  def confirm_email
    @user = User.find_by_confirmation_code(params[:code])
    if @user
      @user.update_columns(is_active: true, confirmation_code: nil)
      redirect_to BaseUrl
    else
      render_not_found('User')
    end
  end
  
  def show
  end
  
  def my_trips    
    @trips = current_user.trips.published.includes(:likes, :images, :comments, :moments).includes(milestones: :moments).order("created_at DESC")        
    render :trips
  end
  
  def liked_trips
    @trip_ids = @user.likes.limit(30).collect(&:trip_id)
    @trips = Trip.get_trips_from_ids(@trip_ids, 30)
    render :trips
  end
  
  def my_drafts
    @trips = current_user.trips.drafts
    render :trips
  end

  def upload_image
    @image = @user.image || @user.build_image
    @image.attributes = {attachment: params[:attachment]}
    if @image.save
      redirect_to BaseUrl + "/html/profile_edit.html"
    else
      render_error_message(@image)
    end
  end
  
  def follow
    @follow = current_user.relationships.find_or_create_by!(followed: @user) unless current_user == @user
    render_nothing
  end

  def unfollow
    @unfollow = @user.reverse_relationships.where(follower: current_user).first
    @unfollow.destroy if @unfollow
    render_nothing
  end
  
  def followers
    @people = current_user.followers
    render :people
  end
  
  def following
    @people = current_user.following.includes(:trips)
    render :people
  end
  
  def following_locations
    @locations = current_user.following_locations
    render 'locations/index'
  end
  
  # Following is for other user's profile
  def following_people
    @people = @user.following.includes(:trips)
    render :people
  end

  # Other user's profile followers
  def followers_other
    @people = @user.followers
    render :people
  end
  
  # Following places on other's user profile
  def following_places
    @locations = @user.following_locations
    render 'locations/index'
  end
  
  
  def feed
    @trips = @user.feeds if @user
    @trips = current_user.feeds unless @trips
    if @trips.blank?
      @suggested_locations = Location.suggested(current_user.id, 30)
      @suggested_users = current_user.suggested_users(30)
    end
    render :trips
  end

  def trips
    @trips = @user.trips.published
  end
  
  def suggest_users
    @followed = current_user.followed_user_ids.push(current_user.id)
    @people = User.find_by_sql("SELECT u.*, (5 * count(r.user_id)+r.like_count) AS order_index FROM users u INNER JOIN (SELECT trips.id AS trip_id, trips.user_id, COUNT(likes.trip_id) AS like_count FROM trips INNER JOIN likes ON likes.trip_id = trips.id     GROUP BY trips.id) r ON u.id = r.user_id WHERE u.id NOT IN (#{@followed.join(',')}) GROUP BY u.id ORDER BY order_index DESC LIMIT 30;")
    render :people
  end

  private
  def user_params
    params["user"].delete "location_attributes" if (params["user"] && params["user"]["location_attributes"] && (params["user"]["location_attributes"]["name"]).blank?)
    params[:user].permit(:name, :email, :password, :password_confirmation, :username, :bio, :weburl,
                         location_attributes: [:name], authentications_attributes: authentications_params)
  end

  def authentications_params
    [:link, :id, :provider, :uid, :is_visible]
  end

  def reset_password_params
    params[:user].permit(:password, :password_confirmation)
  end
  
  def notification_setting_params
    params[:notification_setting].permit(:user_id, :default_as_miles, :like, :comment, :follow, :weekly_digest)
  end

  def load_user    
    @user = User.friendly.find_by_id(params[:id])
    unless @user    
      render_not_found('User')
    end
  end
  
end
