class PagesController < ApplicationController
  
  skip_before_filter :authenticate_user, only: [:twitter_feeds] 
  
  def twitter_feeds
    client = Twitter::REST::Client.new do |config|
      config.consumer_key        = TwitterCred["consumer_key"]
      config.consumer_secret     = TwitterCred["consumer_secret"]
      config.access_token        = TwitterCred["access_token"]
      config.access_token_secret = TwitterCred["access_token_secret"]
    end
    @tweets = client.user_timeline({count: 10}) if client
  end
  
  def search
    query = params[:searchq]
    @locations = Location.search "2% #{query}", include: [:followers], order: {milestone_count: :desc}
    @trips = Trip.search "2% #{query}", fields: [:title, :description, :slug], boost_by: {view_count: {factor: 2}, likes_count: {factor: 10}}, include: [:user, :milestones => :location], where: {is_published: true, is_private: false}
    if (@locations.size > 5 and @trips.size > 5)
      @locations = @locations.first(5)
      @trips = @trips.first(5)
    else
      if @locations.size > @trips.size
        @locations = @locations.first(10-@trips.size)
      else
        @trips = @trips.first(10-@locations.size)
      end
    end
  end

end
