class SessionsController <  Devise::SessionsController

  after_filter :update_first_visit, only: [:create]
  skip_before_filter :authenticate_user

  def create
    logger.debug "xxxxxxxxxxxxxx"
    resource = warden.authenticate!(:scope => resource_name, :recall => "#{controller_path}#failure")
    return sign_in_and_redirect(resource_name, resource)
  end 

  def sign_in_and_redirect(resource_or_scope, resource=nil)
    scope = Devise::Mapping.find_scope!(resource_or_scope)
    resource ||= resource_or_scope
    @user = resource
    if @user && !@user.is_active
      render json: {errors: "Please confirm your account"}, status: 422
      return
    else
      sign_in(scope, @resource) unless warden.user(scope) == @user
      @user.generate_auth_token
      @show_auth_credentials = true
      if @user.first_visit
        @suggested_locations = Location.suggested(@user.id, 30)
        @suggested_users = @user.suggested_users(30)
        @suggested_trips = Trip.suggested(30)
      end
      render "users/show"
    end
  end
 
  def failure    
    render json: {errors: "Invalid credentials"}, status: 401, head: "Unauthorised"
  end

  
  
  def destroy
    user = User.find_by(auth_token: params[:auth_token])
    if user && user.update_attribute(:auth_token, nil)
      render_nothing
    else
      render json: {errors: "User already logged out or does not exist."}, status: 403
    end
  end

  private
  def update_first_visit
    @user.update_attribute(:first_visit, false) if @user
  end
  
end
