class OmniauthCallbacksController < Devise::OmniauthCallbacksController
  skip_before_filter :authenticate_user
  
  def self.provides_callback_for(provider)    
    class_eval %Q{
      def #{provider}    
        @user = User.find_for_oauth(env["omniauth.auth"], current_user)
        if @user.persisted?
          sign_in(@user)
          @user.generate_auth_token
          @show_auth_credentials = true
          if @user.first_visit
            @suggested_locations = Location.suggested(@user.id, 30)
            @suggested_users = @user.suggested_users(30)
            @suggested_trips = Trip.suggested(30)
          end
          render "users/show", content_type: "application/javascript"
        else
          render json: {errors: "Invalid credentials"}, status: 401, head: "Unauthorised", content_type: "application/javascript"
        end
      end
    }
  end

  [:facebook, :google_oauth2].each do |provider|
    provides_callback_for provider
  end

  def after_omniauth_failure_path_for(resource)
    BaseUrl
  end

  def after_sign_in_path_for(resource)
    if resource.email_verified?
      super resource
    else
      finish_signup_path(resource)
    end
  end
end

