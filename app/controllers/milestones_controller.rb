class MilestonesController < ApplicationController
  
  
  def create
    @trip = Trip.where(id: params[:trip_id]).includes(:milestones => :moments).first
    if current_user && @trip && current_user.id == @trip.user_id
      @milestone = @trip.milestones.new(milestone_params)
      @milestone.position = @trip.get_new_position(@milestone.position) if @milestone.position.present?
      if @milestone.save
        @trip.reload
        render 'trips/show'
      else
        render_error_message(@milestone)
      end
    else
      render json: {errors: "Trip not found!"}
    end
  end
  
  def update
    @milestone = Milestone.find_by_id(params[:id])
    if @milestone
      @milestone.update_attributes(milestone_params)
      render :show
    else
      render json: {errors: "Milestone not found"}
    end
  end
  
  def upload_image
    @milestone = Milestone.find_by_id(params[:id])
    @image = @milestone.images.build
    @image.attributes = {attachment: params[:attachment]}
    if @image.save
      @trip = @milestone.trip
      render 'trips/show'
    else
      render_error_message(@image)
    end
  end

  def delete_image
    @image = Image.find_by_id(params[:id])
    if @image.destroy
      render_nothing
    else
      render json: {errors: "Unable to Delete the image", status: 422, head: "Unprocessable Entity"}
    end
  end

  def destroy
    @milestone = Milestone.find_by_id(params[:id])
    @trip = @milestone.trip
    @trip.update_milestone_position(@milestone.position)
    if @milestone.destroy
      render_nothing
    else
      render json: {errors: "Unable to Delete the milestone", status: 422, head: "Unprocessable Entity"}
    end
  end


  private
  def milestone_params
    params[:milestone].permit(:latitude, :longitude, :location_id, :trip_id, :reached_on, :position, :featured, :description, :location_name, :title, :road_condition)
  end
  
end
