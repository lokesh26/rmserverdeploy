class LocationsController < ApplicationController

  before_filter :load_location, except: [:index]
  
  def follow
    @follow = @location.relationships.find_or_create_by(follower: current_user)
    render_nothing
  end

  def unfollow
    @unfollow = @location.relationships.where(follower: current_user).first
    @unfollow.destroy if @unfollow
    render_nothing
  end
  
  def index
    @locations = Location.suggested(current_user.id, 30)
  end

  def search
  end

  private
  def load_location
    @location = Location.find_by_id(params[:id])
    unless @location
      render_not_found('Location')
    end
  end
  
end
