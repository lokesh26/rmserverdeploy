class MomentsController < ApplicationController
  
  def create
    @milestone = Milestone.find_by_id(params[:milestone_id])
    if @milestone
      @moment = @milestone.moments.new(moment_params)
      save_trip
    else
      render json: {errors: "Milestone not found"}
    end
  end

  def create_via_trip
    @trip = Trip.find_by_id(params[:trip_id])
    if @trip
      @moment = @trip.moments.new(moment_params)
      save_trip
    else
      render json: {errors: "Trip not found"}
    end
  end
  
  def save_trip
    if @moment.save
      render :show
    else
      render_error_message(@moment)
    end
  end
  
  def show
    @moment = Moment.find_by_id(params[:id])
    if @moment
      render :show
    else
      render json: {errors: "Moment not found"}
    end
  end
  
  def update
    @moment = Moment.find_by_id(params[:id])
    if @moment
      @moment.update_attributes(moment_params)
      render :show
    else
      render json: {errors: "Moment not found"}
    end
  end
  
  def upload_image
    @moment = Moment.find_by_id(params[:id])
    @image = @moment.images.build
    @image.attributes = {attachment: params[:attachment]}
    if @image.save
      render :show
    else
      render_error_message(@image)
    end
  end

  def delete_image
    @image = Image.find_by_id(params[:id])
    if @image.destroy
      render_nothing
    else
      render json: {errors: "Unable to Delete the image", status: 422, head: "Unprocessable Entity"}
    end
  end

  def destroy
    @moment = Moment.find_by_id(params[:id])
    if @moment.destroy
      render_nothing
    else
      render json: {errors: "Unable to Delete the moment", status: 422, head: "Unprocessable Entity"}
    end
  end

  
  private
  def moment_params
    params[:moment].permit(:location_name, :milestone_id, :trip_id, :category_id, :sub_category_id, :reached_on, :position, :after_milestone, :featured, :description, :title)
  end

end
