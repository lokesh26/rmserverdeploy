class CategoriesController < ApplicationController

  def index
    @categories = Category.categories
  end
  
end
