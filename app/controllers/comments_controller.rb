class CommentsController < ApplicationController
  
  before_filter :load_comment, except: [:report_abuse]
  
  def create
    @comment = @trip.comments.new(comment_params)
    @comment.user = current_user
    if @comment.save
      render :show
    else
      render_error_message(@comment)
    end
  end
  
  def report_abuse
    @comment = Comment.find_by_id(params[:comment_id])
    if @comment
      @comment.is_abused = true
      @comment.report_abuses.build(report_abuse_params)
      if @comment.save
        render_nothing
      else
        render_error_message @comment
      end
    end
  end
  
  private
  def comment_params
    params[:comment].permit(:body)
  end
  
  def report_abuse_params
    params[:comment].merge!(user_id: current_user.id)
    params[:comment].permit(:reason, :remarks, :user_id)
  end
  
  def load_comment
     @trip = @trip = Trip.friendly.find(params[:trip_id])
     unless @trip
       render_not_found('Trip')
     end
  end
  
end
