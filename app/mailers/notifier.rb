class Notifier < ActionMailer::Base
  default from: "support@roadmojo.com"
  
  def confirmation_email(user)
    @user = user
    mail(to: @user.email, subject: "Welcome to RoadMojo")
  end

  def send_password_reset_instructions(user)
    @user = user
    mail(to: user.email, subject: "Roadmojo : Temporary Password")
  end
  
  def alert_team_roadmojo_about_report_abuse(report_abuse)
    @report_abuse = report_abuse
    @comment = report_abuse.comment
    @commentor = report_abuse.comment.user
    @reporter = report_abuse.user
    mail(to: "siddhant.bhardwaj16@gmail.com", subject: "Roadmojo : Comment Abuse Reported")
  end
  
  def send_activity_notification(notification)
    @notification = notification
    @subject = notification.subject
    @actor = notification.actor
    @action = notification.action
    @trip = @action.trip if @action.try(:trip_id)
    mail(to: @subject.email, subject: "Roadmojo : Notification")
  end

  def send_invite(user, email, message)
    @user = user
    @email = email
    @message = message
    mail(from: @user.email ,to: @email, subject: "Roadmojo : Invite"  )
  end
  
end
