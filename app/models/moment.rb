class Moment < ActiveRecord::Base
  searchkick

  ## ATTRIBUTE ACCESSORS ##
  attr_accessor :location_name, :sub_category_id
  
  ## ASSOCIATIONS ##
  belongs_to :momentable, polymorphic: true
  belongs_to :location
  has_many :images, as: :source, :dependent => :delete_all
  belongs_to :category

  ## CALLBACKS ##
  before_save :save_location
  before_save :save_category
  
  def save_category
    self.category_id = self.sub_category_id
  end
  
  def save_location
    self.location = Location.find_or_create_by(name: self.location_name) if location_name
  end
end
