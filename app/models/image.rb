class Image < ActiveRecord::Base

  has_attached_file :attachment, styles: lambda {
    |attachment| {
      original: (
        attachment.instance.source_type.eql?("Trip")? ["1200x800>"] :
        attachment.instance.source_type.eql?("User")? ["200x200>"] : 
        attachment.instance.source_type.eql?("Milestone")? ["700x500>"] :
        attachment.instance.source_type.eql?("Moment")? ["700x500>"] :["10x10>"]
      ),
      medium: (
        attachment.instance.source_type.eql?("Trip")? ["500x300>"] : ["10x10"]
      )
    }
  }
  
  ## VALIDATIONS ##
  validates :source_id, :source_type, :attachment_file_name, presence: true
  validates_attachment :attachment, content_type: { content_type: ["image/jpg", "image/jpeg", "image/png"]}
    
  ## ASSOCIATIONS ##
  belongs_to :source, polymorphic: true

  def cropping?
    !crop_x.blank? && !crop_y.blank? && !crop_w.blank? && !crop_h.blank?
  end

  def attachment_url
    "#{attachment.url}"
  end


end
