class Authentication < ActiveRecord::Base
  
  ## ASSOCIATIONS ##
  belongs_to :user
  
  ## VALIDATIONS ##
  validates :provider, :uid, :user_id, presence: true
  
  
  
end
