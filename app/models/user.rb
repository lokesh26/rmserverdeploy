class User < ActiveRecord::Base
  extend FriendlyId
  TEMP_EMAIL_PREFIX = 'user@roadmojo'
  TEMP_EMAIL_REGEX = /\Auser@roadmojo/

  friendly_id :username, use: :slugged
  searchkick

  devise :database_authenticatable, :registerable, :validatable, :omniauthable,:omniauth_providers => [:facebook, :google_oauth2]


  ## SCOPE ##
  scope :active, -> { where(is_active: true) }

  ## VALIDATIONS ##
  validates :username, presence: true
  # validates :email, format: { with: /\A([^@\s]+)@((?:[-a-z0-9]+\.)+[a-z]{2,})\Z/i, allow_blank: true }, uniqueness: true
  validates :username, uniqueness: { allow_blank: true }

  ## ASSOCIATIONS ##
  has_many :trips, dependent: :destroy
  has_one :image, as: :source
  has_one :notification_setting
  has_many :comments
  has_many :likes
  belongs_to :location
  has_many :relationships, foreign_key: "follower_id", dependent: :destroy
  has_many :reverse_relationships, foreign_key: "followed_id", class_name: "Relationship"
  has_many :followers, through: :reverse_relationships, foreign_key: 'follower_id'
  has_many :following, through: :relationships, source: :followed, source_type: 'User'
  has_many :following_locations, through: :relationships, source: :followed, source_type: 'Location'
  has_many :authentications
  has_many :notifications, as: :subject, dependent: :destroy


  accepts_nested_attributes_for :location
  accepts_nested_attributes_for :authentications

  ## CALLBACKS ##
  before_create :generate_confirmation_code
  after_create :send_confirmation_email
  after_create :set_notification_setting

  ## INSTANCE METHODS ##
  def generate_auth_token
    unless auth_token
      self.update_attribute(:auth_token, SecureRandom.hex)
    end
  end

  def followers
    reverse_relationships.where(followed_type: 'User').collect(&:follower)
  end

  def confirm_email_link
    BaseApiUrl+"/users/#{confirmation_code}/confirm_email"
  end

  def self.find_for_oauth(auth, signed_in_resource = nil)
    identity = Identity.find_for_oauth(auth)

    user = signed_in_resource ? signed_in_resource : identity.user


    if user.nil?
      email_is_verified = auth.info.email && (auth.info.verified || auth.info.verified_email)
      email = auth.info.email if email_is_verified
      user = User.where(:email => email).first if email


      if user.nil?

        user = User.new(
          username: SecureRandom.hex,
          email: email ? email : "#{TEMP_EMAIL_PREFIX}-#{auth.uid}-#{auth.provider}.com",
          password: Devise.friendly_token[0,20],
          has_username: false
        )
        user.save!
      end
    end
    if identity.user != user
      identity.user = user
      identity.save!
    end
    user
  end

  def email_verified?
    self.email && self.email !~ TEMP_EMAIL_REGEX
  end

  def suggested_users(count=5)
    User.joins(:trips).where("users.id != ?", id).order("trips.view_count DESC").limit(count).uniq
  end

  def profile_link_url
    BaseUrl + "/html/profile_view.html?_loc=otherProfile&id=#{friendly_id || id}"
  end

  def image_url
    image.present? ? "#{image.attachment.url}" : ""
  end

  def feeds
    Trip.unscoped.published.joins(:milestones).where("user_id in (?) || milestones.location_id in (?)", followed_user_ids, followed_location_ids).order("published_at DESC").uniq.limit(30)
  end

  def followed_user_ids
    relationships.collect(&:followed_id)
  end

  def followed_location_ids
    Relationship.where(followed_type: 'Location', follower_id: id).collect(&:followed_id)
  end

  def send_password_reset_instructions
    code = SecureRandom.hex(8)
    self.update_attributes(password: code, password_confirmation: code, reset_code: code)
=begin
    user = User.find_by_reset_code(code)
    if user && user != self
      send_password_reset_instructions
    else
      self.update_attribute(:reset_code, code)
=end

    Notifier.send_password_reset_instructions(self).deliver
=begin
      return true
    end
=end

  end

  ## PRIVATE METHODS ##
  private
  def generate_confirmation_code
    self.confirmation_code = SecureRandom.hex
  end

  def send_confirmation_email
    Notifier.confirmation_email(self).deliver
  end

  def set_notification_setting
    NotificationSetting.create(user_id: id)
  end

end
