class Location < ActiveRecord::Base
  searchkick
  
  geocoded_by :name
  after_validation :geocode, :if => :name_changed?
  
  ## VALIDATIONS ##
  validates :name, presence: true
  
  ## ASSOCIATIONS ##
  has_many :users
  has_many :milestones
  has_many :moments
  has_many :notifications, as: :subject
  has_many :relationships, as: :followed
  has_many :followers, through: :relationships
  
  def search_data
    {
      name: name,
      milestone_count: milestones.size # boost this location according to milestones count
    }
  end
  
  
  ## CLASS METHODS ##
  class << self
    def suggested(user_id, count=5)
      followed_location_ids = Relationship.where(follower_id: user_id, followed_type: "Location").collect(&:followed_id)
      Location.includes(:followers).where('locations.id NOT IN (?)', followed_location_ids.present? ? followed_location_ids : [0]).select("distinct locations.*, COUNT(milestones.id) as milestone_count").joins(milestones: :trip).where("trips.is_published = ?", true).group("locations.id").order("milestone_count DESC").limit(count).uniq
    end

    def get_trip_details(location_id=nil)
      location = if location_id
        Location.find_by_id(location_id)
      else
        Location.find_by_name("Delhi")
      end
      if location
        recent_trip = Milestone.joins(:trip).where("trips.is_published IS true").where(location_id: location.id).order("created_at DESC").limit(1).first.try(:trip) || []
        trip_ids = Milestone.where(location_id: location.id).collect(&:trip_id)
        trending_trips = Trip.cached_trending_trips.where(id: trip_ids).sample(2)
        if trending_trips.blank?
          trending_trips = Trip.published.where(id: trip_ids).sample(2)
        end
        return location, recent_trip, trip_ids, trending_trips
      end
    end

  end
  
  def roadtripsCount
    Milestone.joins(:trip).where("trips.is_published IS true").where(location_id: id).collect(&:trip_id).uniq.size
  end

end
