class Milestone < ActiveRecord::Base
  searchkick
  
  scope :by_position, -> {order('position ASC')}
  
  attr_accessor :location_name

  ## VALIDATIONS ##
  validates :position, presence: true, uniqueness: { scope: :trip_id, allow_blank: true }
  
  ## ASSOCIATIONS ##
  belongs_to :trip
  belongs_to :location
  has_many :moments, as: :momentable, :dependent => :delete_all
  has_many :images, as: :source, :dependent => :delete_all

  ## NESTED ATTRIBUTES ##
  accepts_nested_attributes_for :moments
  
  before_save :save_location
  before_save :update_road_condition
  after_save :update_miles_covered

  after_destroy :change_miles_covered
  
  def save_location
    self.location = Location.find_or_create_by(name: self.location_name) if location_name
  end
  
  def update_miles_covered
    bounds = siblings
    if bounds.size == 2
      trip_owner = self.trip.user
      initial_distance = trip.distance.to_i
      distance_subtracted = calculate_distance(bounds.first, bounds.last)
      distance_added1 = calculate_distance(bounds.first, self)
      distance_added2 = calculate_distance(bounds.last, self)
      final_distance = initial_distance + distance_added1 + distance_added2 - distance_subtracted
      if trip.update_attributes(distance: final_distance)
        trip_owner.update_attributes(distance_covered: trip_owner.distance_covered - initial_distance + final_distance)
      end
    end
  end

  def change_miles_covered
    bounds = siblings
    if bounds.size == 2
      trip_owner = self.trip.user
      initial_distance = trip.distance.to_i
      distance_added = calculate_distance(bounds.first, bounds.last)
      distance_subtracted1 = calculate_distance(bounds.first, self)
      distance_subtracted2 = calculate_distance(bounds.last, self)
      final_distance = initial_distance + distance_added - distance_subtracted1 - distance_subtracted2
      if trip.update_attributes(distance: final_distance)
        trip_owner.update_attributes(distance_covered: trip_owner.distance_covered - initial_distance + final_distance)
      end
    end
  end

  def update_road_condition
    if road_condition.blank? && siblings.present?
      self.road_condition = siblings.first.road_condition
    end
  end

  def siblings
    Milestone.by_position.where(trip_id: trip_id).where(position: [position.to_i + 1, position.to_i - 1])
  end

  def calculate_distance(initial_point, end_point)
    Geocoder::Calculations.distance_between([initial_point.location.latitude, initial_point.location.longitude], [end_point.location.latitude, end_point.location.longitude])
  end

end
