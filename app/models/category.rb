class Category < ActiveRecord::Base
  scope :categories, -> {where(parent_id: nil)}  
  has_many :sub_categories, class_name: "Category", foreign_key: "parent_id", dependent: :destroy
  belongs_to :parent_category, class_name: "Category", foreign_key: "parent_id"
  has_many :moments  
end
