class Notification < ActiveRecord::Base
  
  ## VALIDATIONS ##
  validates :subject_id, :actor_id, :action_id, :action_type, presence: true
  
  ## ASSOCIATIONS ##
  belongs_to :subject, polymorphic: true
  belongs_to :actor, class_name: 'User'
  belongs_to :action, polymorphic: true
  
  after_create :send_notification_mail
  
  def send_notification_mail
    if subject.is_a?(User)
      permit_email_to_be_sent = case action_type
      when "Like"
        subject.notification_setting.like
      when "Relationship"
        subject.notification_setting.follow
      when "Comment"
        subject.notification_setting.comment
      end
      Notifier.delay.send_activity_notification(self) if permit_email_to_be_sent && (subject != actor)
    end
  end
  

end
