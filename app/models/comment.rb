class Comment < ActiveRecord::Base
  
  ## VALIDATIONS ##
  validates :user_id, :trip_id, :body, presence: true
  validates_associated :report_abuses
  
  ## ASSOCIATIONS ##
  belongs_to :user
  belongs_to :trip
  has_many :notifications, as: :action
  has_many :report_abuses, class_name: 'ReportAbuse'
  
  ## CALLBACKS ##
  after_create :generate_notification

  ## PRIVATE METHODS ##
  private

  def generate_notification
    self.notifications.create(subject: trip.user, actor_id: user_id)
  end
  
end
