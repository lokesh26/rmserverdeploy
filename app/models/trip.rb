class Trip < ActiveRecord::Base
  extend FriendlyId
  friendly_id :title_source_destination, use: [:slugged, :history]
  searchkick

  ## ATTRIBUTE ACCESSORS ##
  attr_accessor :start_point, :end_point

  ## VALIDATIONS ##
  validates :user_id, presence: true
  validate :check_date_validity
  validates :duration, presence: true

  ## ASSOCIATIONS ##
  belongs_to :user
  has_many :milestones, -> { order(:position => :asc) },dependent: :destroy
  has_many :moments, as: :momentable, dependent: :destroy
  has_many :images, as: :source, dependent: :destroy
  has_many :comments, dependent: :destroy
  has_many :likes, dependent: :destroy

  ## CALLBACKS ##
  after_create :create_initial_milestones
  after_create :update_distance_after_adding_initial_milestones
  before_create :set_published_as_false
  before_validation :set_duration
  after_destroy :reduce_user_distance_covered

  ## NESTED ATTRIBUTES ##
  accepts_nested_attributes_for :milestones

  ## SCOPE ##
  default_scope { order('published_at DESC, created_at DESC') }
  scope :published, -> {where(is_published: true)}
  scope :drafts, -> {where(is_published: false)}

  ## CLASS METHODS ##
  class << self
    def suggested(count=5)
      order("trips.view_count DESC").limit(count).uniq
    end

    def get_trips_from_ids(trip_ids, count=5)
      published.where(id: trip_ids).includes(:likes, :images, :milestones, :comments).order("created_at DESC").limit(count)
    end
  end

  def self.random
    random = rand(Trip.count)
    random = 1 if random.zero?
    offset(random).first
  end

  ## INSTANCE METHODS ##
  def check_date_validity
    if end_date && start_date && end_date.to_date - start_date.to_date < 0
      errors.add(:end_date, "Can't be less than start date")
    end
  end

  def title_source_destination
    "#{title}-#{milestones.by_position.first.location.name}-#{milestones.by_position.last.location.name}" if title.present? && milestones.present?
  end

  def create_initial_milestones
    milestones.create(location: Location.find_or_create_by(name: start_point), position: 1, reached_on: start_date) if start_point
    milestones.create(location: Location.find_or_create_by(name: end_point), position: 2, reached_on: end_date) if end_point
  end

  def update_distance_after_adding_initial_milestones
    return if milestones.size == 0
    distance_between_initial_milestones = Geocoder::Calculations.distance_between([milestones.first.location.latitude, milestones.first.location.longitude], [milestones.last.location.latitude, milestones.last.location.longitude])
    if distance_between_initial_milestones > 0
      self.update_attributes(distance: distance_between_initial_milestones)
      user.update_attributes(distance_covered: user.distance_covered + distance_between_initial_milestones)
    end
  end

  def reduce_user_distance_covered
    user.update_attributes(distance_covered: user.distance_covered - distance)
  end

  def set_published_as_false
    self.is_published = false
    return true
  end

  def get_new_position(position)
    milestones.where("position > ?", position).order("position desc").update_all("position = position + 1")
    return position+1;
  end

  def update_milestone_position(position)
    milestones.where("position > ?", position).update_all("position = position - 1")
    return
  end

  def set_duration
    self.duration = (end_date.to_date - start_date.to_date).to_i if ((start_date && end_date) && !duration.present?)
  end

  def get_view_count
    view_count.to_i
  end

  def milestones_count
    milestones.size
  end

  def cover_image_url
    cover_image.present? ? "#{cover_image}" : ""
  end

  def trip_url
    "#{BaseUrl}/html/road_trip_view?id=#{friendly_id || id}"
  end

  def moments_count
    milestone_moments_count = 0
    milestones.includes(:moments).each do |milestone|
      milestone_moments_count += milestone.moments.size
    end
    return (moments.size + milestone_moments_count)
  end

  def images_count
    image_count = 0
    milestones.includes(:moments).each do |milestone|
      image_count += milestone.images.size
      milestone.moments.each do |moment|
        image_count += moment.images.size
      end
    end
    moments.each do |independent|
      image_count += independent.images.size
    end
    cover = images.last ? 1 : 0
    return (cover + image_count)
  end

  def comments_count
    comments.size
  end

  def get_transport_mode
    t_mode = TRANSPORT_MODES.find{|mode| mode[self.transport_mode] }
    t_mode.blank? ? "" : t_mode[self.transport_mode]
  end

  def cover_image
    images.present? ? images.last.attachment.url : ""
  end

  def cover_image_item
    images.present? ? images.last.attachment.url(:medium) : ""
  end

  def self.cached_trending_trips(current_user = nil)
    if current_user
      Rails.cache.fetch([name, "trending_trips#{current_user.try(:id)}"], expires_in: 1.day){
        following_loc_trip_ids = Milestone.where(id: current_user.following_location_ids).collect(&:trip_id)
        liked_trip_ids = Like.where(created_at: 2.weeks.ago.beginning_of_day..Date.today.end_of_day ).collect(&:trip_id)
        Trip.unscoped.published.select("distinct trips.*, COUNT(likes.trip_id) as likes_count").joins(:likes).group("likes.trip_id").where(id: (following_loc_trip_ids+liked_trip_ids).uniq).order("likes_count DESC, published_at DESC")
      }
    else
      Rails.cache.fetch([name, "trending_trips_global"], expires_in: 1.day){
        Trip.unscoped.published.select("distinct trips.*, COUNT(likes.trip_id) as likes_count").joins(:likes).group("likes.trip_id").where("likes.created_at > ?", 2.weeks.ago.beginning_of_day).order("likes_count DESC, published_at DESC")
      }
    end
  end

  def self.featured
    cached_trending_trips.limit(3)
  end

end
