class ReportAbuse < ActiveRecord::Base
  ## VALIDATIONS ##
  validates :user_id, :comment_id, :reason, presence: true
  
  ## ASSOCIATIONS ##
  belongs_to :comment
  belongs_to :user
  
  after_create :alert_team_roadmojo
  
  def alert_team_roadmojo
    Notifier.delay.alert_team_roadmojo_about_report_abuse(self)
  end

end
