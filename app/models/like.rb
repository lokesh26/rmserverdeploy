class Like < ActiveRecord::Base
  
  ## VALIDATIONS ##
  validates :user_id, :trip_id, presence: true
  
  ## ASSOCIATIONS ##
  belongs_to :user
  belongs_to :trip, counter_cache: true
  has_many :notifications, as: :action
  
  ## CALLBACKS ##
  after_create :generate_notification

  ## PRIVATE METHODS ##
  private

  def generate_notification
    self.notifications.create(subject: trip.user, actor_id: user_id)
  end

end
