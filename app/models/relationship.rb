class Relationship < ActiveRecord::Base
  
  ## VALIDATIONS ##
  validates :follower_id, :followed_id, :followed_type, presence: true
  
  ## ASSOCIATIONS ##
  belongs_to :follower, class_name: 'User'
  belongs_to :followed, polymorphic: true
  has_many :notifications, as: :action

  ## CALLBACKS ##
  after_create :generate_notification

  def generate_notification
    self.notifications.create(subject: followed, actor_id: follower_id)
  end

end
