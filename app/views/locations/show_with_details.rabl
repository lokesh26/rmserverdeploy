object @location
attributes :id, :name, :latitude, :longitude

node :roadtripsCount do |location|
  location.roadtripsCount
end
node :followingCount do |location|
  location.followers.size
end