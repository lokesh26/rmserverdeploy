object false

node :trips do
  @trips.map do |trip|
    milestones = trip.milestones
    {
      title: trip.title, 
      username: trip.user ? (trip.user.name || "") : "",
      screenName: trip.user.username,
      user_id: trip.user ? (trip.user.id || "") : "",
      first_milestone: milestones.first.location ? milestones.first.location.name.split(",").first : "",
      last_milestone: milestones.last.location ? milestones.last.location.name.split(",").first : "",
      trip_url: trip.trip_url
    }
  end
end

node :locations do
  @locations.map do |location|
    {
      id: location.id,
      name: location.name,
      is_followed: location.followers.include?(current_user),
      followersCount: location.followers.size,
      roadtripsCount: location.roadtripsCount
    }
  end
end