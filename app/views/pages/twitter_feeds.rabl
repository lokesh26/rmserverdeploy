collection @tweets, object_root: false
attributes :text
node :username do |tweet|
  tweet.user.name
end
node :profile_background_image_url do |tweet|
  tweet.user.profile_background_image_url
end