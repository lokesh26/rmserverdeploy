logged_in_user = current_user 
user_liked_trips = logged_in_user ? logged_in_user.likes.collect(&:trip_id) : []
user_commented_trips = logged_in_user ? logged_in_user.comments.collect(&:trip_id) : []

if @suggested_locations.present? || @suggested_users.present?
	node :suggested_locations do
		@suggested_locations.map do |location|
			{id: location.id, name: location.name}
		end
	end

	node :suggested_users do
		@suggested_users.map do |user|
			{id: user.id, name: user.name, email: user.email}
		end
	end
else
	collection @trips, object_root: false
	extends "trips/trip_item", locals: { user_liked_trips: user_liked_trips, user_commented_trips: user_commented_trips }
end