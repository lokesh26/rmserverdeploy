collection @people
attributes :id, :name, :username
node :desc do |user|
  user.bio
end
node :roadtripsCount do |user|
  user.trips.published.size
end
node :followingCount do |user|
  user.followers.count
end
node :is_followed do |user|
  current_user.following.collect(&:id).include?(user.id)
end
node :tripMiles do |user|
  user.trips.published.collect(&:distance).sum
end
node :userPic do |user|
  user.image_url
end