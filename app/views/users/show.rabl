object @user
attributes :id,:email, :name, :weburl, :bio, :username, :friendly_id, :has_username
node :userPic do
  @user.image_url
end
if current_user
  node :is_followed do
    @user.follower_ids.include?(current_user.id)
  end
  
  node :is_followed do
    @user.follower_ids.include?(current_user.id)
  end
end
node :following_users do
  @user.following.limit(11).map do |following_user|
    {
      id: following_user.id,
      userPic: following_user.image_url
    }
  end
end


node :location_name do
  @user.location.try(:name)
end
node :location_id do
  @user.location.try(:id)
end
node :distance_covered do
  @user.trips.published.collect(&:distance).sum
end
node :followers do
  @user.followers.size
end
node :published_trips do |trip|
  @user.trips.published.count
end
node :saved_drafts do |trip|
  @user.trips.drafts.count
end
node :following do
  @user.following.size
end
node :following_locations do
  @user.following_locations.size
end
node :authentications do
	@user.authentications.map do |authentication|
    if authentication.is_visible
      {link: authentication.link, provider: authentication.provider, is_visible: true}
    else
      {link: "", provider: authentication.provider, is_visible: false}
    end
	end
end

if @show_auth_credentials || current_user == @user
  attributes :auth_token
  child :notification_setting do
    attributes :default_as_miles, :like, :comment, :follow, :weekly_digest
  end
end
if @suggested_locations.present?
	node :suggested_locations do
		@suggested_locations.map do |location|
			{id: location.id, name: location.name}
		end
	end
end
if @suggested_users.present?
	node :suggested_users do
		@suggested_users.map do |user|
			{id: user.id, name: user.name, email: user.email}
		end
	end
end
if @suggested_trips.present?
	node :suggested_trips do
		@suggested_trips.map do |trip|
			{title: trip.title, id: trip.id}
		end
	end
end