node :notifications do
  @notifications.map do |noti|
    generate_notification_content(noti)
  end
end
