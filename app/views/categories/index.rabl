collection @categories
attributes :id, :name
child :sub_categories => "sub_categories" do
  attributes :id, :name
end