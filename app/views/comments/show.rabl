object @comment
attributes :id, :body
child :user do
	attributes :id, :name, :username
  node :userPic do |user|
    user.image_url
  end
end
node :timeAgo do |comment|
  time_ago_in_words(comment.created_at)
end