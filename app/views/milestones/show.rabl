object @milestone
attributes :reached_on, :position, :featured, :description, :road_condition
child :trip do
 attributes :id, :title
end
node :title do |milestone|
	milestone.location.try(:name) ? milestone.location.name.split(",").first : ""
end
node :location do |milestone|
	milestone.location.try(:name)
end
node :images do |milestone|
  milestone.images.collect{|img| img.attachment_url}
end
