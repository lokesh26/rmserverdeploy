object @moment
attributes :id, :title, :reached_on, :featured, :description
node :location do |moment|
	moment.location.try(:name)
end
node :images do |moment|
  moment.images.collect{|img| img.attachment_url}
end
node :category_id do |moment|
  moment.category.parent_category.as_json(only: [:id, :name]) if moment.category
end
node :sub_category_id do |moment|
  moment.category.as_json(only: [:id, :name])
end

if root_object.momentable.is_a?(Trip)
  node :after_milestone do |moment|
     moment.after_milestone
  end
end

if root_object.momentable.is_a?(Milestone)
  node :at_milestone do |moment|
    moment.momentable_id
  end
end

node :images do |moment|
  arr = []
  moment.images.each{|image| arr << image.attachment_url}
  arr
end