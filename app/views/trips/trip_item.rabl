	attributes :id, :user_id, :friendly_id, :published_at
	
  node :tripName do |trip|
		trip.title
	end
  
  node :tripDesc do |trip|
		trip.description
	end
  
  node :userPic do |trip|
		trip.user ? trip.user.image_url : ""
	end
	
  node :userName do |trip|
		trip.user.try(:name)
	end

  node :screenName do |trip|
		trip.user.username
	end
	
  node :num_miles do |trip|
		trip.distance
	end
  
  node :minsAgo do |trip|
		trip.is_published? ? (time_ago_in_words(trip.published_at) if trip.published_at.present?) : time_ago_in_words(trip.created_at)
	end
	
  node :noMoments do |trip|
		trip.moments_count
	end
	
  node :likes do |trip|
		trip.likes.size
	end
	
  node :comments do |trip|
		trip.comments_count
	end
	
  node :noPhotos do |trip|
		trip.images_count
	end
	
  node :noViews do |trip|
		trip.get_view_count
	end

  node :tripPic do |trip|
		trip.cover_image_item
	end
	
  node :noMilestones do |trip|
		trip.milestones_count
	end
	
  node :isLiked do |trip|
    locals[:user_liked_trips].include?(trip.id)
	end
	
  node :hasCommented do |trip|
    locals[:user_commented_trips].include?(trip.id)
  end
  
  node :locations do |trip|
    milestones = trip.milestones
    start_location = milestones.first.try(:location)
    end_location = milestones.last.try(:location)
    if start_location && end_location
      node :locations do
        {:start_point => partial("locations/show", :object => start_location), :end_point => partial("locations/show", :object => end_location) }
      end
    end
  end