user_liked_trips = current_user ? current_user.likes.collect(&:trip_id) : []
user_commented_trips = current_user ? current_user.comments.collect(&:trip_id) : []

object false
child @trending_trips => :trending_trips do
  extends "trips/trip_item", locals: { user_liked_trips: user_liked_trips, user_commented_trips: user_commented_trips }
end

child @recent_trip => :recent_trips do
  extends "trips/trip_item", locals: { user_liked_trips: user_liked_trips, user_commented_trips: user_commented_trips }
end

node :location_details do
  {:location_name => @location.name, :roadtrips_count => @trip_ids.size, :followers_count => @location.followers.count}
end
node :is_followed do
  current_user ? current_user.following_locations.include?(@location) : false
end