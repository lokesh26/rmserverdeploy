collection @trips
attributes :id, :name, :description, :is_published, :friendly_id, :published_at, :user_id
node :userPic do |trip|
	trip.user.image_url
end
node :userName do |trip|
	trip.user.name
end
node :screenName do |trip|
	trip.user.username
end
node :num_miles do |trip|
	trip.distance
end
node :minsAgo do |trip|
	time_ago_in_words(trip.created_at)
end
node :num_moments do |trip|
	trip.moments_count
end
node :num_likes do |trip|
	trip.likes.size
end
node :num_comments do |trip|
	trip.comments_count
end
node :num_photos do |trip|
	trip.images_count
end
node :views_count do |trip|
	trip.get_view_count
end
node :mplink do
	""
end
node :tripView do |trip|
	 'road_trip_view.html'
end
node :tripPic do |trip|
	trip.cover_image
end
node :num_milestones do |trip|
	trip.milestones_count
end
node :locations do |trip|
  milestones = trip.milestones
  start_location = milestones.first.location
  end_location = milestones.last.location
  
  node :locations do
    {:start_point => partial("locations/show", :object => start_location), :end_point => partial("locations/show", :object => end_location) }
  end
end