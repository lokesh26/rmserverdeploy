object @trip
attributes :id, :title, :friendly_id, :description, :transport_mode, :is_published, :published_at, :user_id

node :locations do |trip|
  milestones = trip.milestones
  node :locations do
    {:start_point => partial("locations/show", :object => milestones.first.location), :end_point => partial("locations/show", :object => milestones.last.location) }
  end
end

node :start_date do |trip|
	trip.start_date ? trip.start_date.strftime("%Y-%m-%d") : ""
end

node :end_date do |trip|
	trip.end_date ? trip.end_date.strftime("%Y-%m-%d") : ""
end

node :num_days do |trip|
	trip.duration
end

node :num_miles do |trip|
	trip.distance
end

node :num_milestones do |trip|
	trip.milestones_count
end

node :num_moments do |trip|
	trip.moments_count
end

node :likes do |trip|
  trip.likes.size
end

node :photos do |trip|
	trip.images_count
end

node :view_count do |trip|
	trip.get_view_count
end

node :coverImg do |trip|
	trip.cover_image_url
end
logged_in_user = current_user 


node :isLiked do |trip|
  user_liked_trips = current_user ? current_user.likes.collect(&:trip_id) : []
  user_liked_trips.include?(trip.id)
end

node :hasCommented do |trip|
  user_commented_trips = current_user ? current_user.comments.collect(&:trip_id) : []
  user_commented_trips.include?(trip.id)
end

ordered_milestones = @trip.milestones
child :milestones do
  attributes :id, :description, :reached_on, :position, :featured, :road_condition
  node :title do |milestone|
    milestone.location.try(:name) ? milestone.location.name.split(",").first : ""
  end
  node :next_milestone do |milestone|
    next_milestone = ordered_milestones[ordered_milestones.index(milestone) + 1]
    next_milestone.location.name.split(",").first if next_milestone && next_milestone.location
  end
  node :location_name do |milestone|
    milestone.location.name  if milestone.location
  end
  node :latitude do |milestone|
    milestone.location.latitude if milestone.location
  end
  node :longitude do |milestone|
    milestone.location.longitude  if milestone.location
  end
  
  node :images do |milestone|
    arr = []
    milestone.images.each{|image| arr << image.attachment_url}
    arr
  end
  
  child :moments do |moment|
    extends 'moments/show'
  end
end

child :moments do |moment|
  extends 'moments/show'
end

node :num_comments do |trip|
  trip.comments.size
end

child :comments do |comment|
   extends 'comments/show'
end

node :is_following do |user|
  if current_user 
    current_user.following.collect(&:id).include?(user.id)
  end
end

node :current_userpic do |user|
  if current_user
    current_user.image_url
  end
end
